import argparse
import copy
import hashlib
import itertools as it
import json
import queue
import sys
from pathlib import Path

from aalpy.automata import MealyState, MooreState
from aalpy.base import AutomatonState, DeterministicAutomaton
from aalpy.learning_algs import run_Alergia
from aalpy.utils import moore_from_state_setup

from pmsatlearn import run_pmSATLearn


def bisimilar(a1: DeterministicAutomaton, a2: DeterministicAutomaton):
    """
    Checks whether the provided moore machines are bisimilar
    """

    to_check = queue.Queue[tuple[AutomatonState, AutomatonState]]()
    to_check.put((a1.initial_state, a2.initial_state))
    requirements = dict()
    requirements[(a1.initial_state, a2.initial_state)] = []

    while not to_check.empty():
        s1, s2 = to_check.get()

        if (isinstance(s1, MooreState) and s1.output != s2.output) or (
            isinstance(s1, MealyState) and s1.output_fun != s2.output_fun
        ):
            return requirements[(s1, s2)]

        t1, t2 = s1.transitions, s2.transitions
        for t in it.chain(t1.keys(), t2.keys()):
            if (t in t1.keys()) != (t in t2.keys()):
                return requirements[(s1, s2)] + [t]

        for t in t1.keys():
            c1, c2 = t1[t], t2[t]
            if (c1, c2) not in requirements:
                requirements[(c1, c2)] = requirements[(s1, s2)] + [t]
                to_check.put((c1, c2))


def server():
    mm = moore_from_state_setup(
        {
            "offline": ("offline", {"connect": "online", "disconnect": "offline"}),
            "online": ("online", {"connect": "online", "disconnect": "offline"}),
        }
    )
    traces = [
        ["offline", ("disconnect", "offline"), ("connect", "online")],
        [
            "offline",
            ("connect", "online"),
            ("connect", "online"),
            ("disconnect", "offline"),
        ],
    ]
    return mm, traces


def server_glitch():
    mm = moore_from_state_setup(
        {
            "offline": ("offline", {"connect": "online", "disconnect": "offline"}),
            "online": ("online", {"connect": "online", "disconnect": "offline"}),
        }
    )
    traces = [
        ["offline", ("disconnect", "offline"), ("connect", "online")],
        [
            "offline",
            ("connect", "online"),
            ("connect", "online"),
            ("disconnect", "offline"),
        ],
        ["offline", ("disconnect", "offline"), ("connect", "offline")],
    ]
    return mm, traces


def ping_pong():
    mm = moore_from_state_setup(
        {
            "off": ("off", {"connect": "ack", "ping": "off"}),
            "ack": ("ack", {"connect": "off", "ping": "pong"}),
            "pong": ("pong", {"connect": "ack2", "ping": "pong"}),
            "ack2": ("ack", {"connect": "off", "ping": "off"}),
        }
    )
    assert mm.is_minimal(), "Example 2 is not minimal"
    return (
        mm,
        None,
    )


def parse_args(cmd_args):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "example",
        choices=["ping_pong", "server", "server_glitch"],
        help="Example to run on",
    )
    args = parser.parse_args(cmd_args)
    return args


def main(cmd_args: list[str] | None = None):
    args = parse_args(sys.argv[1:] if cmd_args is None else cmd_args)
    if args.example == "server":
        funccall = server
    elif args.example == "server_glitch":
        funccall = server_glitch
    elif args.example == "ping_pong":
        funccall = ping_pong

    mm, traces = funccall()
    if traces is None:
        print(f"INFO: Example {args.example} does not have preprepared traces")
        print("INFO: Please take existing traces from artifacts, or generate new ones")
        name = f"example_{args.example}"
        mm.save(name)
        exit()

    n_states = len(mm.states)
    inputs = mm.get_input_alphabet()
    outputs = sorted(set(state.output for state in mm.states))

    original_traces = copy.deepcopy(traces)

    ## prepare traces and visualization

    traces_hash = hashlib.md5(
        str(traces).encode("utf8"), usedforsecurity=False
    ).hexdigest()
    path = Path("EXAMPLE-results") / traces_hash
    path.mkdir(parents=True, exist_ok=True)

    with (path / "info.json").open("w") as f:
        json.dump(
            {
                "num_states": n_states,
                "input_alphabet": inputs,
                "output_alphabet": outputs,
                "original_traces": original_traces,
                "traces": traces,
            },
            f,
        )

    mm.save((path / "RealModel").as_posix())

    simple_moore_traces = [
        ([trace[0]] + [tuple(entry[:2]) for entry in trace[1:]])
        for trace in traces
        if len(trace) > 1
    ]

    ## Learn with IO ALERGIA
    alergia_learned = run_Alergia(simple_moore_traces, "mdp", print_info=True, eps=0.5)
    alergia_learned.save((path / "alergiaLearned").as_posix())

    ## Try different timeouts for PartialMaxSAT Algorithm

    max_states = n_states + 10
    result_det_pmsat_automaton = [None] * (max_states + 1)
    for n in range(len(outputs), max_states + 1):
        pmstrat = "rc2"  # "lsu", "fm", "rc2"

        # n = n_states + 6
        print(f"Learning with N={n}")
        pmsat_learned, pmsat_learned_stoc, info = run_pmSATLearn(
            traces,
            n,
            automata_type="moore",
            pm_strategy=pmstrat,
            timeout=None,
            print_info=False,
        )
        result_det_pmsat_automaton[n] = pmsat_learned
        name = f"pmsatLearned-{pmstrat}-N{n}-RN{n_states}"
        if pmsat_learned is not None:
            assert pmsat_learned_stoc is not None
            print("#glitches:", len(info["glitch_steps"]))
            pmsat_learned.save((path / name).as_posix())
            stoc_name = (path / (name + "-STOC")).as_posix()
            pmsat_learned_stoc.save(stoc_name)
            try:
                with open(stoc_name + ".dot", "r") as f:
                    dot_stoc_automaton = f.read()
                with open(stoc_name + ".dot", "w") as f:
                    for i in range(1, 1000):
                        if f'] t{i}"];' not in dot_stoc_automaton:
                            break
                        dot_stoc_automaton = dot_stoc_automaton.replace(
                            f'] t{i}"];', ']"];'
                        )
                    f.write(
                        dot_stoc_automaton.replace('label="!', 'color=red, label="')
                    )
            except FileNotFoundError:
                print("WARN: Could not find file for stochastic edge coloring red")

            cex = bisimilar(mm, pmsat_learned)
            info["bisimilar_base_cex"] = cex
            if cex is not None:
                print("Is not bisimilar, cex:", cex)
            for automaton_index, learned_pmsat_automaton in enumerate(
                result_det_pmsat_automaton
            ):
                if learned_pmsat_automaton is not None:
                    if bisimilar(pmsat_learned, learned_pmsat_automaton) is None:
                        info["bisimilar_to_pmsat_with_states"] = automaton_index
                        break
            assert info["bisimilar_to_pmsat_with_states"]
        else:
            print("PMSAT - UNSAT")
        with (path / (name + ".json")).open("w") as f:
            json.dump(info, f)
        if info["optimal_solution"] is False:
            print("Breaking from loop due to interrupt or timeout")
            break
        if "cost" in info and info["cost"] == 0:
            print(
                "Found glitchless solution, more states will not change that. Stopping..."
            )
            break


if __name__ == "__main__":
    main()
