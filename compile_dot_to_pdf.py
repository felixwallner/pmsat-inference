import argparse
import os
import subprocess
import sys
from pathlib import Path

from tqdm import tqdm


def find_all_files_with_suffix(path, suffix):
    """Returns a set of relative paths of files ending with suffix"""
    files = set()
    for r, d, f in os.walk(path):
        for file in f:
            if file.endswith(suffix):
                files.add(os.path.join(r, file))
    return files


def parse_args(cmd_args):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "folders_or_files", nargs="+", type=Path, help="Traces to learn from"
    )
    args = parser.parse_args(cmd_args)
    return args


def main(cmd_args: list[str] | None = None):
    try:
        subprocess.run(["dot", "-V"], check=True)
    except FileNotFoundError:
        print(
            "Please install 'graphviz' to access the 'dot' command for compilation",
            file=sys.stderr,
        )
        exit(1)
    args = parse_args(sys.argv[1:] if cmd_args is None else cmd_args)
    files = set()
    for folder_or_file in args.folders_or_files:
        if not folder_or_file.exists():
            print(
                f"The file or folder {folder_or_file.as_posix()} does not exist",
                file=sys.stderr,
            )
            exit(2)
        if folder_or_file.is_dir():
            files |= set(
                Path(p) for p in find_all_files_with_suffix(folder_or_file, ".dot")
            )
        elif folder_or_file.is_file() and folder_or_file.suffix == ".dot":
            files.add(folder_or_file)
        else:
            print(f"WARN: ignoring {folder_or_file.as_posix()}")
    for file in tqdm(files):
        result = file.with_suffix(".pdf")
        subprocess.run(
            ["dot", "-Tpdf", file.as_posix(), "-o", result.as_posix()], check=True
        )


if __name__ == "__main__":
    main()
