# BUILDER
FROM ubuntu:22.04 as builder-image
RUN apt-get update && apt-get install --no-install-recommends -y python3.10 python3.10-dev python3.10-venv python3-pip python3-wheel build-essential && apt-get clean && rm -rf /var/lib/apt/lists/*

# create virtual environment
RUN python3.10 -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH" 

# install requirements
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# RUNNER
FROM ubuntu:22.04 as runner-image
RUN apt-get update && apt-get install --no-install-recommends -y python3.10 python3-venv graphviz && apt-get clean && rm -rf /var/lib/apt/lists/

VOLUME /pmsat-inference
WORKDIR /pmsat-inference

COPY --from=builder-image /opt/venv /opt/venv

# activate virtual environment
ENV VIRTUAL_ENV=/opt/venv
ENV PATH="/opt/venv/bin:$PATH"

CMD ["bash"]