import copy
import hashlib
import itertools as it
import json
import multiprocessing
import os
import queue
import random
import sys
import time
from pathlib import Path
from pprint import pp
from typing import Literal

import matplotlib.pyplot as plt
import numpy as np
from aalpy.automata import Mdp, MdpState, MealyState, MooreMachine, MooreState
from aalpy.base import SUL, AutomatonState, DeterministicAutomaton, Oracle
from aalpy.learning_algs import run_Alergia, run_Lstar
from aalpy.oracles import RandomWalkEqOracle
from aalpy.utils import load_automaton_from_file
from tqdm import tqdm

from f_similarity import f_score

try:
    from tikzplotlib import save as tikz_save
except ImportError:
    tikz_save = lambda *args, **kwargs: None
    print("WARN: tikzplotlib is not installed, no tikz plots will be created")


def find_all_files_with_suffix(path, suffix):
    """Returns a set of relative paths of files ending with suffix"""
    files = set()
    for r, d, f in os.walk(path):
        for file in f:
            if file.endswith(suffix):
                files.add(os.path.join(r, file))
    return files


def stochastic_to_moore(mdp: Mdp) -> MooreMachine:
    states = {state.state_id: MooreState(i, state.output) for i, state in enumerate(mdp.states)}
    for s, state in zip(states.values(), mdp.states):
        for key, targets in state.transitions.items():
            if not targets:
                continue
            target, highest_prob = sorted(targets, key=lambda x: x[1])[-1]
            assert all(prob <= highest_prob for _, prob in targets)
            assert key not in s.transitions
            s.transitions[key] = states[target.state_id]
    return MooreMachine(states[mdp.initial_state.state_id], list(states.values()))


def main():
    random.seed(42)  # for reproducability

    args = sys.argv
    if len(args) != 2:
        print("USAGE: python evaluate_results_alergia_fscore.py FOLDER_WITH_RESULTS")
        exit(-1)

    path = Path(args[1])
    if not path.exists():
        print(f"Path {path.as_posix()} is invalid or does not exist")
        exit(-1)

    files = set(Path(p) for p in find_all_files_with_suffix(path, ".json"))
    info_files = [file for file in files if file.name == "info.json"]
    if len(info_files) == 0:
        print(f"No 'info.json' files found")
        exit(-1)
    if len(info_files) == 1:
        print(f"Only single 'info.json' file found. Correct folder?")
        exit(-1)

    infos = []
    for file in info_files:
        with file.open("r") as f:
            infos.append((file, json.load(f)))
    print(f"{len(infos)} info files found")

    # infos = [(infofile, info) for infofile, info in infos if info["fault_type"] == "discard"]
    # infos = [(infofile, info) for infofile, info in infos if info["glitch_percent"] == 0.01]
    # infos = [(infofile, info) for infofile, info in infos if info["num_states"] == 8]
    # infos = [(infofile, info) for infofile, info in infos if len(info["output_alphabet"]) == 5]
    # print(f"INFO: {len(infos)} info files after FILTERING")

    acc = dict()
    for infofile, info in tqdm(infos):
        real = infofile.parent / "RealModel.dot"
        assert real.exists()
        real_aut = load_automaton_from_file(real, "moore")
        assert real_aut.is_minimal()
        pmlearned = infofile.parent / f"pmsatLearned-N{real_aut.size}.dot"
        if pmlearned.exists():
            pm_aut = load_automaton_from_file(pmlearned, "moore")
            f_pm = f_score(real_aut, pm_aut)
        else:
            print("Does not exist:", pmlearned)
            f_pm = 0

        results = [f_pm]

        simple_moore_traces = [
            ([trace[0]] + [tuple(entry[:2]) for entry in trace[1:]])
            for trace in info["traces"]
            if len(trace) > 1
        ]

        ## Use different settings for IO ALERGIA
        for eps in [0.005, 0.5, "auto"]:
            path = infofile.parent

            alergia_learned = run_Alergia(simple_moore_traces, "mdp", print_info=False, eps=eps)
            alergia_learned_det = stochastic_to_moore(alergia_learned)

            f_al_auto = f_score(real_aut, alergia_learned)
            results.append(f_al_auto)
            f_al_det_auto = f_score(real_aut, alergia_learned_det)
            results.append(f_al_det_auto)

        n = info["num_states"]
        # fault_type = info["fault_type"]

        key = n
        acc[key] = acc.get(key, []) + [results]

    with open("evaluate_results_alergia_fscore.json", "w") as f:
        json.dump(acc, f)
    pp({key: (acc[key], len(acc[key])) for key in sorted(acc.keys())})

    fig, ax = plt.subplots()

    all_ns = sorted(set(n for n in acc.keys()))
    # all_faults = sorted(set(fault for _, fault in acc.keys()))
    for f_index, f_score_type in enumerate(
        [
            "pmsat",
            "alergia-auto",
            "alergiaDet-auto",
            "alergia-0.5",
            "alergiaDet-0.5",
            "alergia-0.005",
            "alergiaDet-0.005",
        ]
    ):
        # vals = [np.sum(acc[(n, fault_type)]) * 10 for n in all_ns]
        vals = [np.mean([all_fscores[f_index] for all_fscores in acc[n]]) for n in all_ns]
        plt.plot(all_ns, vals, "-*", label=f"{f_score_type}")

    plt.xlabel("number of states")
    # plt.ylabel("mean solve time [min]")
    plt.ylabel("mean f-score")
    # plt.ylabel("percentage of correctly inferred models")
    plt.ylim((-0.02, 1.02))

    plt.grid(True)
    plt.legend()
    plt.savefig("figure3f.png")
    plt.show()
    # TODO: required a change in tikzplotlib/_legend.py draw_legend:
    # if hasattr(obj, "_ncol"): ... else: _ncols ...
    tikz_save("tikzplot.tex", figure=fig, strict=True)


if __name__ == "__main__":
    main()
