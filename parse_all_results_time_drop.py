import json
import os
import sys
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np


def find_all_files_with_suffix(path, suffix):
    """Returns a set of relative paths of files ending with suffix"""
    files = set()
    for r, d, f in os.walk(path):
        for file in f:
            if file.endswith(suffix):
                files.add(os.path.join(r, file))
    return files


def main():
    args = sys.argv
    if len(args) != 2:
        print("USAGE: python parse_results.py FOLDER_WITH_RESULTS")
        exit(-1)

    path = Path(args[1])
    if not path.exists():
        print(f"Path {path.as_posix()} is invalid or does not exist")
        exit(-1)

    files = set(Path(p) for p in find_all_files_with_suffix(path, ".json"))
    info_files = [file for file in files if file.name == "info.json"]
    other_json_files = files.difference(info_files)
    if len(info_files) == 0:
        print("No 'info.json' files found")
        exit(-1)
    if len(info_files) == 1:
        print("Only single 'info.json' file found. Correct folder?")
        exit(-1)

    infos = []
    for file in info_files:
        with file.open("r") as f:
            infos.append((file, json.load(f)))
    print(f"{len(infos)} info files found")

    acc = dict()
    totalsolve = []
    for infofile, info in infos:
        gp = info["glitch_percent"]
        learninfofiles = [
            file for file in other_json_files if file.parent == infofile.parent
        ]
        if len(learninfofiles) != 1:
            print(f"WARN: {infofile} -> {learninfofiles}")
            continue
        with learninfofiles[0].open("r") as f:
            learninfo = json.load(f)
        n = learninfo["num_states"]
        solve_time = learninfo["solve_perf_counter"]
        totalsolve.append(solve_time)
        key = (n, gp)
        acc[key] = acc.get(key, []) + [solve_time]

    fig, ax = plt.subplots()

    all_ns = sorted(set(n for n, _ in acc.keys()))
    all_gps = sorted(set(gp for _, gp in acc.keys()))

    for gp in all_gps:
        vals = [np.mean(acc[(n, gp)]) / 60 for n in all_ns]
        plt.plot(all_ns, vals, "-*", label=f"dropped {gp}")

    plt.xlabel("number of states")
    plt.ylabel("mean solve time [min]")
    plt.ylim((-2, 62))

    plt.grid(True)
    plt.legend()
    plt.savefig("figure3c.png")
    plt.show()


if __name__ == "__main__":
    main()
