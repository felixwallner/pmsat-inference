import json
import os
import sys
from pathlib import Path

import numpy as np


def find_all_files_with_suffix(path, suffix):
    """Returns a set of relative paths of files ending with suffix"""
    files = set()
    for r, d, f in os.walk(path):
        for file in f:
            if file.endswith(suffix):
                files.add(os.path.join(r, file))
    return files


def main():
    args = sys.argv
    if len(args) != 2:
        print("USAGE: python parse_results.py FOLDER_WITH_RESULTS", file=sys.stderr)
        exit(-1)

    path = Path(args[1])
    if not path.exists():
        print(f"Path {path.as_posix()} is invalid or does not exist", file=sys.stderr)
        exit(-1)

    files = set(Path(p) for p in find_all_files_with_suffix(path, ".json"))
    info_files = [file for file in files if file.name == "info.json"]
    other_json_files = files.difference(info_files)
    if len(info_files) == 0:
        print("No 'info.json' files found", file=sys.stderr)
        exit(-1)
    if len(info_files) == 1:
        print("Only single 'info.json' file found. Correct folder?", file=sys.stderr)
        exit(-1)

    infos = []
    for file in info_files:
        with file.open("r") as f:
            infos.append((file, json.load(f)))
    print(f"{len(infos)} info files found", file=sys.stderr)

    acc = dict()
    totalsolve = []
    for infofile, info in infos:
        output_len = len(info["output_alphabet"])
        learninfofiles = [
            file for file in other_json_files if file.parent == infofile.parent
        ]
        if len(learninfofiles) != 1:
            print(f"WARN: {infofile} -> {learninfofiles}")
            continue
        with learninfofiles[0].open("r") as f:
            learninfo = json.load(f)
        n = learninfo["num_states"]
        timed_out = learninfo["timed_out"]
        solve_time = learninfo["solve_perf_counter"]
        totalsolve.append(solve_time)
        key = n
        acc[key] = acc.get(key, []) + [timed_out]

    print(
        "total solve time",
        sum(totalsolve),
        "mean",
        np.mean(totalsolve),
        "timeouts",
        sum(1 for s in totalsolve if s > 3600),
        file=sys.stderr,
    )
    all_ns = sorted(set(n for n in acc.keys()))
    sums = [np.sum(acc[n]) for n in all_ns]
    aj = lambda x: (f"{x:.3}" if isinstance(x, float) else str(x)).ljust(7)
    print(*list(map(aj, ["n"] + all_ns)), sep=",")
    print(*list(map(aj, ["# t.o."] + sums)), sep=",")
    print(*list(map(aj, ["% t.o."] + [s / sum(sums) for s in sums])), sep=",")


if __name__ == "__main__":
    main()
