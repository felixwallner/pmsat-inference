import json
import os
import sys
from pathlib import Path
from pprint import pp

import matplotlib.pyplot as plt
import numpy as np

try:
    from tikzplotlib import save as tikz_save
except ImportError:
    tikz_save = lambda *args, **kwargs: None
    print("WARN: tikzplotlib is not installed, no tikz plots will be created")


def find_all_files_with_suffix(path, suffix):
    """Returns a set of relative paths of files ending with suffix"""
    files = set()
    for r, d, f in os.walk(path):
        for file in f:
            if file.endswith(suffix):
                files.add(os.path.join(r, file))
    return files


def main():
    args = sys.argv
    if len(args) != 2:
        print("USAGE: python parse_results_faults.py FOLDER_WITH_RESULTS")
        exit(-1)

    path = Path(args[1])
    if not path.exists():
        print(f"Path {path.as_posix()} is invalid or does not exist")
        exit(-1)

    files = set(Path(p) for p in find_all_files_with_suffix(path, ".json"))
    info_files = [file for file in files if file.name == "info.json"]
    other_json_files = files.difference(info_files)
    if len(info_files) == 0:
        print(f"No 'info.json' files found")
        exit(-1)
    if len(info_files) == 1:
        print(f"Only single 'info.json' file found. Correct folder?")
        exit(-1)

    infos = []
    for file in info_files:
        with file.open("r") as f:
            infos.append((file, json.load(f)))
    print(f"{len(infos)} info files found")

    acc = dict()
    for infofile, info in infos:
        fault_type = info["fault_type"]
        learninfofiles = [
            file for file in other_json_files if file.parent == infofile.parent
        ]
        if len(learninfofiles) != 1:
            print(f"WARN: {infofile} -> {learninfofiles}")
            continue
        with learninfofiles[0].open("r") as f:
            learninfo = json.load(f)
        sat = learninfo["is_sat"]
        n = learninfo["num_states"]
        timed_out = learninfo["timed_out"]
        solve_time = learninfo["solve_perf_counter"]
        correct = sat == True and learninfo["bisimilar_base_cex"] is None
        # key = n
        # key = (n, output_len)
        key = (n, fault_type)
        acc[key] = acc.get(key, []) + [solve_time]
        # acc[key] = acc.get(key, []) + [correct]

    pp({key: (acc[key], len(acc[key])) for key in sorted(acc.keys())})

    fig, ax = plt.subplots()

    # all_ns = sorted(set(n for n, _ in acc.keys()))
    # all_os = sorted(set(olen for _, olen in acc.keys()))
    # for olen in all_os:
    #     valid_ns = [n for n in all_ns if n >= olen]
    #     # vals = [np.mean(acc[(n, olen)]) / 60 for n in valid_ns]  # solve time
    #     vals = [np.mean(acc[(n, olen)]) for n in valid_ns]
    #     plt.plot(valid_ns, vals, "-*", label=f"|O| = {olen}")

    all_ns = sorted(set(n for n, _ in acc.keys()))
    all_faults = sorted(set(fault for _, fault in acc.keys()))
    for fault_type in all_faults:
        # vals = [np.sum(acc[(n, fault_type)]) * 10 for n in all_ns] % * 10 for percent
        vals = [np.mean(acc[(n, fault_type)]) / 60 for n in all_ns]
        plt.plot(all_ns, vals, "-*", label=f"{fault_type}")

    # all_ns = sorted(set(n for n, _ in acc.keys()))
    # all_tls = sorted(set(tl for _, tl in acc.keys()))
    # print("all tls", all_tls)
    # # tls = sorted(set(round(tl / 10) * 10 for tl in all_tls))
    # # print("tls", tls)
    # for tl in all_tls:
    #     val_m = [np.clip(acc[(n, tl)], 0, 3600) for n in all_ns]
    #     print(val_m, type(val_m), len(val_m))
    #     # vals = [np.mean(acc[(n, tl)]) if (n, tl) in acc else -1 for n in all_ns]
    #     # vals = [np.sum(acc[(n, gp)]) for n in all_ns]
    #     vals = [np.mean(v) / 60 for v in val_m]
    #     plt.plot(all_ns, vals, "-*", label=f"tlen {tl}")
    # # all_tls = sorted(set(tl for tl in acc.keys()))
    # # print("all tls", all_tls, len(all_tls))
    # # for key in acc.keys():
    # #     acc[key] = np.clip(acc[key], 0, 3600)
    # # # tls = sorted(set(round(tl / 10) * 10 for tl in all_tls))
    # # # print("tls", tls)
    # # plt.plot(all_tls, [np.mean(acc[tl]) for tl in all_tls], "-*", label=f"tlen {tl}")

    # all_ns = sorted(set(n for n in acc.keys()))
    # plt.plot(all_ns, [np.mean(acc[n]) for n in all_ns], "-*")
    # print(all_ns, [np.sum(acc[n]) for n in all_ns])
    # print(all_ns, [np.mean(acc[n]) for n in all_ns])

    # tl_key = sorted(acc.keys())
    # tl_val = [np.mean(acc[key]) for key in tl_key]
    # tl_val = [np.sum(acc[key]) for key in tl_key]
    # fig, ax = plt.subplots()
    # ax.xaxis.set_major_locator(MultipleLocator(1))
    # ax.yaxis.set_major_locator(MultipleLocator(5))
    # ax.yaxis.set_minor_locator(MultipleLocator(1))
    # ax.grid(which="major", color="#CCCCCC", linestyle="--")
    # ax.grid(which="minor", color="#CCCCCC", linestyle=":")
    # plt.plot(tl_key, tl_val, "-*")  # , label="solve time")
    # # plt.title(f"Partial Max-SAT solve time ")
    plt.xlabel("number of states")
    # plt.ylabel("mean solve time [min]")
    plt.ylabel("mean solve time [min]")
    # plt.ylabel("percentage of correctly inferred models")
    # plt.ylim((-2, 62))

    # plt.ylabel("percentage of correct inferrences")
    # plt.ylim((-0.02, 1.02))
    # yvals = ax.get_yticks()
    # ax.set_yticklabels(["{:,.0%}".format(y) for y in yvals], fontsize=12)

    plt.grid(True)
    plt.legend()
    plt.savefig("figure3d.png")
    plt.show()
    # TODO: required a change in tikzplotlib/_legend.py draw_legend:
    # if hasattr(obj, "_ncol"): ... else: _ncols ...
    tikz_save("tikzplot.tex", figure=fig, strict=True)


if __name__ == "__main__":
    main()
