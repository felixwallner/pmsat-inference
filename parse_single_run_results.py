import json
import os
import sys
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import MultipleLocator


def find_all_files_with_suffix(path, suffix):
    """Returns a set of relative paths of files ending with suffix"""
    files = set()
    for r, d, f in os.walk(path):
        for file in f:
            if file.endswith(suffix):
                files.add(os.path.join(r, file))
    return files


def main():
    args = sys.argv
    if len(args) != 2:
        print("USAGE: python parse_results.py FOLDER_WITH_RESULTS", file=sys.stderr)
        exit(-1)

    path = Path(args[1])
    if not path.exists():
        print(f"Path {path.as_posix()} is invalid or does not exist", file=sys.stderr)
        exit(-1)

    files = set(Path(p) for p in find_all_files_with_suffix(path, ".json"))
    info_files = [file for file in files if file.name == "info.json"]
    if len(info_files) != 1:
        print(
            f"Require exactly one 'info.json' file, found {len(info_files)}",
            file=sys.stderr,
        )
        exit(-1)

    with info_files[0].open("r") as f:
        info = json.load(f)

    results = []
    for file in (file for file in files if file.name.startswith("pmsatLearned-")):
        with file.open("r") as f:
            res = json.load(f)
        results.append(res)

    results.sort(key=lambda x: x["num_states"])
    # print(results)

    dominant_delta_freq_label = "dominant_delta_freq"
    glitched_delta_freq_label = "glitched_delta_freq"
    dominant_reachable_states_label = "dominant_reachable_states"
    cost_label = "cost"
    num_states_label = "num_states"
    bisimilar_to_nstates_label = "bisimilar_to_pmsat_with_states"
    dominant_delta_freq = list(map(lambda x: x[dominant_delta_freq_label], results))
    glitched_delta_freq = list(map(lambda x: x[glitched_delta_freq_label], results))
    glitched_delta_freq = [g if g else [0] for g in glitched_delta_freq]
    # print(glitched_delta_freq)
    dominant_reachable_states = list(
        map(lambda x: x[dominant_reachable_states_label], results)
    )
    num_states = list(map(lambda x: x[num_states_label], results))
    costs = list(map(lambda x: x[cost_label], results))
    bisimilarity = list(
        map(
            lambda x: x[bisimilar_to_nstates_label]
            if x[bisimilar_to_nstates_label]
            else x[num_states_label],
            results,
        )
    )

    dominant_delta_freq_mean = list(map(np.mean, dominant_delta_freq))
    dominant_delta_freq_min = list(map(np.min, dominant_delta_freq))
    dominant_delta_freq_median = list(map(np.median, dominant_delta_freq))
    dominant_delta_freq_std = list(map(np.std, dominant_delta_freq))
    glitched_delta_freq_mean = list(map(np.mean, glitched_delta_freq))
    glitched_delta_freq_median = list(map(np.median, glitched_delta_freq))
    glitched_delta_freq_max = list(map(np.max, glitched_delta_freq))
    dominant_delta_missing = [
        sum(el == 0 for el in sequence) for sequence in dominant_delta_freq
    ]
    dominant_delta_outlier = [
        sum(el == 1 for el in sequence) for sequence in dominant_delta_freq
    ]
    glitched_delta_missing = [
        sum(el == 0 for el in sequence) for sequence in glitched_delta_freq
    ]
    # assert all(el == 0 for el in glitched_delta_missing)
    glitched_delta_outlier = [
        sum(el == 1 for el in sequence) for sequence in glitched_delta_freq
    ]

    table = list(
        zip(
            *[
                ["n"] + num_states,
                ["n_reach"] + dominant_reachable_states,
                ["# Glitches"] + costs,
                ["Mean d_g fr."] + glitched_delta_freq_mean,
                ["Max d_g fr."] + glitched_delta_freq_max,
                ["Min d fr."] + dominant_delta_freq_min,
                ["bisimilar to n="] + bisimilarity,
            ]
        )
    )
    for row in table:
        jrow = [
            (f"{e:.3}" if isinstance(e, float) else str(e)).ljust(len(table[0][i]))
            for i, e in enumerate(row)
        ]
        print(",".join(jrow))

    # print("-" * 30)
    # print(glitched_delta_freq, "glitched_delta_freq")
    # print(dominant_reachable_states, "dominant_reachable_states")
    # print(num_states, "num_states")
    # print(costs, "costs")
    # print(dominant_delta_freq_mean, "dominant_delta_freq_mean")
    # print(dominant_delta_freq_std, "dominant_delta_freq_std")
    fig, ax = plt.subplots()
    ax.xaxis.set_major_locator(MultipleLocator(1))
    ax.yaxis.set_major_locator(MultipleLocator(5))
    ax.yaxis.set_minor_locator(MultipleLocator(1))
    ax.grid(which="major", color="#CCCCCC", linestyle="--")
    ax.grid(which="minor", color="#CCCCCC", linestyle=":")
    # plt.errorbar(
    #     num_states,
    #     dominant_delta_freq_mean,
    #     dominant_delta_freq_std,
    #     label=dominant_delta_freq_label,
    # )
    plt.plot(
        num_states,
        dominant_delta_freq_mean,
        "-^",
        label=f"{dominant_delta_freq_mean=}".partition("=")[0],
    )
    plt.plot(
        num_states,
        dominant_delta_freq_median,
        "-^",
        label=f"{dominant_delta_freq_median=}".partition("=")[0],
    )
    plt.plot(
        num_states,
        dominant_delta_freq_min,
        "-^",
        label=f"{dominant_delta_freq_min=}".partition("=")[0],
    )
    plt.plot(
        num_states,
        dominant_delta_missing,
        "-v",
        label=f"{dominant_delta_missing=}".partition("=")[0],
    )
    plt.plot(
        num_states,
        dominant_delta_outlier,
        "-v",
        label=f"{dominant_delta_outlier=}".partition("=")[0],
    )
    plt.plot(
        num_states,
        glitched_delta_freq_mean,
        "-v",
        label=f"{glitched_delta_freq_mean=}".partition("=")[0],
    )
    plt.plot(
        num_states,
        glitched_delta_freq_median,
        "-v",
        label=f"{glitched_delta_freq_median=}".partition("=")[0],
    )
    plt.plot(
        num_states,
        glitched_delta_outlier,
        "-^",
        label=f"{glitched_delta_outlier=}".partition("=")[0],
    )
    plt.plot(
        num_states,
        dominant_reachable_states,
        "-o",
        label=f"{dominant_reachable_states=}".partition("=")[0],
    )
    plt.plot(
        num_states,
        bisimilarity,
        "-*",
        label=f"{bisimilarity=}".partition("=")[0],
    )

    plt.plot(num_states, costs, "-v", label="number_of_glitches")
    result_name = info.get("original_automaton", "BLE").split("/")[-1] + (
        f" (n={info['num_states']})" if "num_states" in info else ""
    )
    plt.title(f"Partial Max-SAT in {result_name}")
    plt.xlabel("number of states")
    # plt.ylabel("cost")
    plt.grid(True)
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
