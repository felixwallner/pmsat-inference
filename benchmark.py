import copy
import hashlib
import itertools as it
import json
import multiprocessing
import queue
import random
import sys
import time
from concurrent.futures._base import TimeoutError as PebbleTimeout
from pathlib import Path
from pprint import pp
from typing import Literal

import pebble
from aalpy.automata import MealyState, MooreMachine, MooreState
from aalpy.base import SUL, AutomatonState, DeterministicAutomaton, Oracle
from aalpy.learning_algs import run_Alergia, run_Lstar
from aalpy.oracles import RandomWalkEqOracle
from aalpy.SULs import MooreSUL
from aalpy.utils import convert_i_o_traces_for_RPNI, generate_random_moore_machine

from pmsatlearn import run_pmSATLearn


class PerfectEqOracle(Oracle):
    def __init__(self, alphabet: list, sul: SUL, mm: MooreMachine):
        super().__init__(alphabet, sul)
        self.mm = mm

    def find_cex(self, hypothesis):
        """
        Return a counterexample (inputs) that displays different behavior on system under learning and
        current hypothesis.

        Args:

          hypothesis: current hypothesis

        Returns:

            tuple or list containing counterexample inputs, None if no counterexample is found
        """
        self.reset_hyp_and_sul(hypothesis)
        # print(hypothesis)
        mm = self.mm  # mm: MooreMachine = copy.deepcopy(self.mm)
        assert set(mm.get_input_alphabet()) == set(
            self.alphabet
        ), f"{mm.get_input_alphabet()} != {self.alphabet}"
        dis = mm.find_distinguishing_seq(mm.current_state, hypothesis.current_state)
        if dis is None:
            return None  # no CEX found
        # print("Found cex:", dis)
        # return dis
        assert self.sul.step(None) == hypothesis.step(None)
        for index, inp in enumerate(dis):
            out1 = self.sul.step(inp)
            out2 = hypothesis.step(inp)
            if out1 != out2:
                assert (
                    index == len(dis) - 1
                ), f"Difference in output not on last index? {index} != {len(dis)-1}"
                return dis
                # print("Found difference in output without exception??")
        assert False, "Did not find difference in output on performing CEX??"


def bisimilar(a1: DeterministicAutomaton, a2: DeterministicAutomaton):
    """
    Checks whether the provided moore machines are bisimilar
    """

    to_check = queue.Queue[tuple[AutomatonState, AutomatonState]]()
    to_check.put((a1.initial_state, a2.initial_state))
    requirements = dict()
    requirements[(a1.initial_state, a2.initial_state)] = []

    while not to_check.empty():
        s1, s2 = to_check.get()

        if (isinstance(s1, MooreState) and s1.output != s2.output) or (
            isinstance(s1, MealyState) and s1.output_fun != s2.output_fun
        ):
            return requirements[(s1, s2)]

        t1, t2 = s1.transitions, s2.transitions
        for t in it.chain(t1.keys(), t2.keys()):
            if (t in t1.keys()) != (t in t2.keys()):
                return requirements[(s1, s2)] + [t]

        for t in t1.keys():
            c1, c2 = t1[t], t2[t]
            if (c1, c2) not in requirements:
                requirements[(c1, c2)] = requirements[(s1, s2)] + [t]
                to_check.put((c1, c2))


class TracedMooreSUL(MooreSUL):
    def __init__(self, mm: MooreMachine) -> None:
        super().__init__(mm)
        self.current_trace = [self.mm.step(None)]
        self.traces = [self.current_trace]

    def pre(self):
        super().pre()
        assert self.mm.current_state == self.mm.initial_state
        if len(self.current_trace) > 1:
            new_trace = [self.mm.step(None)]
            self.traces.append(new_trace)
            self.current_trace = new_trace

    def step(self, input, description: str | None = None):
        output = super().step(input)
        if input is None:
            return output
        if description is not None:
            entry = (input, output, description)
        else:
            entry = (input, output)
        self.traces[-1].append(entry)
        return output


def run(
    # pool: pebble.ProcessPool,
    path: Path,
    seed: int,
    n_states: int,
    glitch_percent: float,
    inputs: list[str],
    outputs: list[str],
    trace_steps: int,
    reset_prob: float = 0.09,
    pmstrat: Literal["lsu", "fm", "rc2"] = "rc2",
    tracegen: Literal["lstar", "rw"] = "rw",
    timeout: int | None = None,
) -> list[dict]:
    random.seed(seed)
    mm: MooreMachine = generate_random_moore_machine(
        n_states, inputs, outputs, compute_prefixes=True, ensure_minimality=True
    )
    # assert mm.is_strongly_connected(), "Generated MooreMachine must be strongly connected"
    assert mm.is_minimal(), "Generated MooreMachine is not minimal"
    sul = TracedMooreSUL(mm)
    if tracegen == "rw":
        random_walk = RandomWalkEqOracle(inputs, sul, num_steps=trace_steps, reset_prob=reset_prob)
        cex = random_walk.find_cex(copy.deepcopy(mm))
        assert cex is None, "Found CEX with identical machines"
    elif tracegen == "lstar":
        oracle = PerfectEqOracle(inputs, sul, mm)
        learned = run_Lstar(inputs, sul, oracle, "moore", cache_and_non_det_check=False)
        cex = bisimilar(mm, learned)
        assert cex is None, "Lstar did not learn bisimilar automaton to src"
    else:
        raise TypeError("Tracegen is invalid")
    traces = sul.traces
    initial_output = traces[0][0]
    assert (
        len(traces) >= 1
        and all(len(trace) >= 1 for trace in traces)
        and all(trace[0] == initial_output for trace in traces)
    )
    original_traces = copy.deepcopy(traces)
    if glitch_percent and glitch_percent > 0:
        if glitch_percent >= 0.5:
            print(f"WARN: trace amount more than {glitch_percent*100}%, are you sure about that?")
            time.sleep(5)
        total_steps = sum(len(trace) - 1 for trace in traces)
        total_num_glitches = int(total_steps * glitch_percent)
        if total_num_glitches <= 0:
            print(f"WARN: adding one glitch (although we would have gotten zero)")
            total_num_glitches = 1
        indices = range(len(traces))
        weights = [len(trace) / total_steps for trace in traces]
        glitch_counts = random.choices(indices, weights, k=total_num_glitches)
        assert len(glitch_counts) == total_num_glitches
        ### this code DROPS random entries from the trace
        for i in range(len(traces)):
            glitch_count = sum(1 for index in glitch_counts if index == i)
            if glitch_count == 0:
                continue
            trace = traces[i]
            if glitch_count >= len(trace) - 1:
                print("WARN: dropping entire trace!")
                glitch_count = len(trace) - 1
            glitch_rm_indices = set(random.sample(range(1, len(trace)), k=glitch_count))
            o0 = trace[0]
            traces[i] = [o0] + [
                entry
                for index, entry in enumerate(trace[1:])
                if (index + 1) not in glitch_rm_indices
            ]
        print(
            f"INFO: removed {total_num_glitches} entries from trace with original total length {total_steps}"
        )
        if sum(len(trace) - 1 for trace in traces) != total_steps - total_num_glitches:
            print(
                "WARN: sums of traces are",
                sum(len(trace) - 1 for trace in traces),
                "!=",
                total_steps - total_num_glitches,
            )
        # assert sum(len(trace) - 1 for trace in traces) == total_steps - total_num_glitches

    ## prepare traces and visualization

    traces_hash = hashlib.md5(str(traces).encode("utf8"), usedforsecurity=False).hexdigest()
    path = path / traces_hash
    path.mkdir(parents=True, exist_ok=True)

    with (path / "info.json").open("w") as f:
        json.dump(
            {
                "glitch_percent": glitch_percent,
                "random_seed": seed,
                "num_states": n_states,
                "input_alphabet": inputs,
                "output_alphabet": outputs,
                "original_traces": original_traces,
                "traces": traces,
                "trace_steps": trace_steps,
                "actual_trace_steps": sum(len(trace) - 1 for trace in traces),
                "reset_prob": reset_prob,
                "pmstrat": pmstrat,
            },
            f,
        )

    mm.save((path / f"RealModel").as_posix())

    simple_moore_traces = [
        ([trace[0]] + [tuple(entry[:2]) for entry in trace[1:]])
        for trace in traces
        if len(trace) > 1
    ]

    ## Use different setings for IO ALERGIA
    try:
        alergia_learned = run_Alergia(simple_moore_traces, "mdp", print_info=True, eps=0.5)
        alergia_learned.save((path / "alergiaLearned").as_posix())
    except ZeroDivisionError:
        with (path / "alergiaZeroDivError").open("w") as f:
            f.write("Zero division error")

    ## Try different timeouts for PartialMaxSAT Algorithm
    n = n_states
    print(f"Learning with N={n}")
    name = f"pmsatLearned-N{n}"

    pmsat_learned, pmsat_learned_stoc, info = run_pmSATLearn(
        traces, n, automata_type="moore", pm_strategy=pmstrat, timeout=timeout, print_info=False
    )
    if pmsat_learned is not None:
        assert pmsat_learned_stoc is not None
        # print("GLITCHES:", info["glitch_steps"])
        print("#glitches:", len(info["glitch_steps"]))
        pmsat_learned.save((path / name).as_posix())
        pmsat_learned_stoc.save((path / (name + "-STOC")).as_posix())
        # pmsat_learned_stoc.visualize()
        cex = bisimilar(mm, pmsat_learned)
        info["bisimilar_base_cex"] = cex
    else:
        print("PMSAT - UNSAT")
        # assert False, "Should always find SAT solution except with timeout or break"
    with (path / (name + ".json")).open("w") as f:
        json.dump(info, f)


def main():
    timeout = 1 * 60 * 60  # one hour timeout
    path = Path(f"BENCHMARKS-results")
    inputs = ["x", "y", "z"]
    reset_prob: float = 0.09
    tracegen = "lstar"  # "rw"
    pmstrat = "rc2"
    outputstring = "abcdefghijklmnopqrstuvw"

    if tracegen == "lstar":
        reset_prob = None
        trace_steps = None

    futures = []
    print(f"Starting {multiprocessing.cpu_count()} tasks...", file=sys.stderr)
    with pebble.ProcessPool() as pool:
        for seed in range(43, 54):  # 42 for rc2 earlier, 43 for songp, 44-47 (incl) for 3 seeds
            for n_outputs in range(2, min(len(outputstring), 10)):
                outputs = list(outputstring[:n_outputs])
                for n_states in range(len(outputs), 18):
                    # for trace_steps in [50, 100, 500, 1000, 2000]:
                    for glitch_percent in [0.0, 0.01, 0.02, 0.05, 0.1]:
                        assert (
                            tracegen == "lstar" or trace_steps is not None
                        ), f"Use trace_steps for rw"
                        # for pmstrat in ["lsu", "fm", "rc2"]:
                        folder = path / f"{n_states=}-{n_outputs=}-{glitch_percent=}"
                        print(f"Scheduling {folder.as_posix()}", file=sys.stderr)
                        future = pool.schedule(
                            run,
                            (
                                folder,
                                seed,
                                n_states,
                                glitch_percent,
                                inputs,
                                outputs,
                                trace_steps,
                                reset_prob,
                                pmstrat,
                                tracegen,
                                timeout,
                            ),
                        )
                        futures.append((folder, future))

        print(
            f"Scheduled {len(futures)} tasks for max timeout {timeout / 60 / 60:.2} hours each running {multiprocessing.cpu_count()} in parallel for total maximum of {len(futures)/multiprocessing.cpu_count() * timeout / 60 / 60} hours runtime",
            file=sys.stderr,
        )
        for index, (folder, future) in enumerate(futures):
            try:
                future.result()
                print(f"Done with task {index}", file=sys.stderr, end="\r")
            except PebbleTimeout:
                print("did NOT expect to get here!")
                with (folder / "timeout.txt").open("w") as f:
                    f.write(f"TIMEOUT after {timeout} seconds")
            except KeyboardInterrupt:
                print("Stopping tasks...", file=sys.stderr)
                pool.stop()
                break
            except Exception:
                import traceback

                with (folder / "exception.txt").open("w") as f:
                    traceback.print_exc(file=f)

        print("", file=sys.stderr)
        print("Waiting for tasks to finish...", file=sys.stderr)
        pool.join(30)
    print("Done here", file=sys.stderr)
    return 0


if __name__ == "__main__":
    exit(main())
