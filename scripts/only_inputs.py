import argparse
import sys
from pathlib import Path

from aalpy.automata import MooreMachine
from aalpy.base import SUL, Oracle
from aalpy.learning_algs import run_Lstar
from aalpy.SULs import MooreSUL
from aalpy.utils import load_automaton_from_file


class PerfectEqOracle(Oracle):
    def __init__(self, alphabet: list, sul: SUL, mm: MooreMachine):
        super().__init__(alphabet, sul)
        self.mm = mm

    def find_cex(self, hypothesis):
        """
        Return a counterexample (inputs) that displays different behavior on system under learning and
        current hypothesis.

        Args:

          hypothesis: current hypothesis

        Returns:

            tuple or list containing counterexample inputs, None if no counterexample is found
        """
        self.reset_hyp_and_sul(hypothesis)
        # print(hypothesis)
        mm = self.mm  # mm: MooreMachine = copy.deepcopy(self.mm)
        # assert set(mm.get_input_alphabet()) == set(
        #     self.alphabet
        # ), f"{mm.get_input_alphabet()} != {self.alphabet}"
        visited = set()

        def get_dis():
            to_explore = [(mm.current_state, hypothesis.current_state, [])]
            alphabet = self.alphabet
            while to_explore:
                (curr_s1, curr_s2, prefix) = to_explore.pop(0)
                visited.add((curr_s1, curr_s2))
                for i in alphabet:
                    o1 = mm.output_step(curr_s1, i)
                    o2 = mm.output_step(curr_s2, i)
                    new_prefix = prefix + [i]
                    if o1 != o2:
                        return new_prefix
                    else:
                        next_s1 = curr_s1.transitions[i]
                        next_s2 = curr_s2.transitions[i]
                        if (next_s1, next_s2) not in visited:
                            to_explore.append((next_s1, next_s2, new_prefix))

        dis = get_dis()
        if dis is None:
            return None  # no CEX found
        # print("Found cex:", dis)
        # return dis
        assert self.sul.step(None) == hypothesis.step(None)
        for index, inp in enumerate(dis):
            out1 = self.sul.step(inp)
            out2 = hypothesis.step(inp)
            if out1 != out2:
                assert (
                    index == len(dis) - 1
                ), f"Difference in output not on last index? {index} != {len(dis)-1}"
                return dis
                # print("Found difference in output without exception??")
        assert False, "Did not find difference in output on performing CEX??"


def parse_args(cmd_args):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "moore_dot_file", type=str, help="Automata to learn and generated trace from"
    )
    parser.add_argument(
        "alphabet",
        nargs="*",
        type=str,
        action="append",
        help="Input alphabet to use",
    )
    args = parser.parse_args(cmd_args)
    return args


def main(cmd_args: list[str] | None = None):
    args = parse_args(sys.argv[1:] if cmd_args is None else cmd_args)
    file = Path(args.moore_dot_file)
    assert file.exists(), f"File {file} does not exist"
    inputs = sorted(set(i for i in args.alphabet[0]))
    assert inputs, f"No inputs {inputs}"
    save_file = file.parent / (file.stem + "_" + "_".join(inputs) + file.suffix)
    mm: MooreMachine = load_automaton_from_file(file, "moore", True)
    print("True inputs:", sorted(mm.get_input_alphabet()))
    print("Self inputs:", sorted(inputs))
    # inputs = sorted(set(mm.get_input_alphabet()))
    outputs = sorted(set(state.output for state in mm.states))
    assert mm.is_minimal(), "Machine was not minimal!"
    assert mm.is_input_complete(), "Machine is not input complete"

    sul = MooreSUL(mm)

    lstar_learned: MooreMachine | None = run_Lstar(
        inputs,
        sul,
        PerfectEqOracle(inputs, sul, mm),
        "moore",
        return_data=False,
        cache_and_non_det_check=True,
    )
    lstar_learned.save(save_file)


if __name__ == "__main__":
    main()
