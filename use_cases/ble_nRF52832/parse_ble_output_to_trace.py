import json


def main():
    with open("nRF52832_moore_without_mtu_req_input_sequences_device_output.txt", "r") as f:
        device_output = f.readlines()

    reset = "------------------------------------------------------------"
    i = "TX ---> "
    o = "RX <--- "
    spl = "/"
    empty = "Empty"

    def gen_output(o_set):
        return "|".join(sorted(o_set)) if len(o_set) > 0 else empty

    traces = []
    trace = [gen_output(set())]
    last_input = None
    last_output = set()

    for line_number, line in enumerate(device_output):
        line = line.strip()
        if line.startswith(i) or line.startswith(o):
            if line.startswith(i):
                # input
                input_symbol = line[len(i) :]
                input_symbol = "|".join(sorted(set(map(str.strip, input_symbol.split(spl)))))
                if last_input is not None:
                    # then this is the first input after some other input output sequence
                    if trace is not None:
                        trace.append((last_input, gen_output(last_output)))
                    last_input, last_output = None, set()
                last_input = input_symbol
            elif line.startswith(o):
                # output
                output_symbol = line[len(o) :]
                last_output.update(map(str.strip, output_symbol.split(spl)))
            else:
                assert False
        else:
            if line.startswith(reset):
                # end of query
                if trace is None:
                    print("WARN:", line_number, line)
                    assert False
                trace.append((last_input, gen_output(last_output)))
                last_input, last_output = None, set()
                traces.append(trace)
                trace = None

                # start of query
                trace = [gen_output(last_output)]
                last_input, last_output = None, set()
            else:
                print("WARN: invalid line:", line_number, line)
                # assert False

    with open("nRF52832_moore_without_mtu_req_parsed_trace_raw.json", "w") as f:
        json.dump(traces, f)

    startouts = set(trace[0] for trace in traces)
    if len(startouts) > 1:
        print("WARN: we have multiple starting outputs (not same starting state)")
        print(startouts)
        assert False

    input_alphabet = set(el[0] for trace in traces for el in trace[1:])
    output_alphabet = set(el[1] for trace in traces for el in trace[1:])
    print("traces", len(traces))
    print("input alphabet", len(input_alphabet), input_alphabet)
    print("output alphabet", len(output_alphabet), output_alphabet)

    input_mapping = {
        "BTLE_SCAN_REQ": "scan_req",
        "BTLE_CONNECT_REQ": "connection_req",
        "LL_LENGTH_RSP": "length_rsp",
        "LL_VERSION_IND": "version_req",
        "ATT_Exchange_MTU_Request": "mtu_req",
        "LL_TERMINATE_IND": "term_req",
        "LL_LENGTH_REQ": "length_req",
        "LL_LENGTH_RSP": "length_rsp",
        "LL_FEATURE_REQ": "feature_req",
        "LL_FEATURE_RSP": "feature_rsp",
        "SM_Pairing_Request": "pairing_req",
    }
    # path outputs are a valid (equivalent) response to scan_req
    output_mapping = {
        "BTLE_ADV_IND": "Adv",
        "BTLE_SCAN_RSP": "Adv",
    }

    original_traces = traces[:]

    traces = [
        [trace[0]]
        + [
            (input_mapping.get(i.split("|")[-1], i), output_mapping.get(o.split("|")[-1], o))
            for i, o in trace[1:]
        ]
        for trace in traces
    ]

    # remove post query (term_req)
    for index, trace in enumerate(traces):
        if any(i == "term_req" for i, _ in trace[1:-1]):
            print("WARN: term_req part of alpabet!")
            print(index, trace[1:-1])
            exit()
    for index, trace in enumerate(list(traces)):
        if trace[-1][0] == "term_req":
            traces[index].pop()
        else:
            print("WARN: trace does not end with term_req")
            print(index, trace)
            exit()

    # remove pre query (scan, conn, scan)
    new_traces = []
    for index, trace in enumerate(traces):
        if len(trace) <= 4:
            print(f"Trace {index} has only length {len(trace)}, removing...")
        elif (
            trace[1][0] != "scan_req"
            or trace[2][0] != "connection_req"
            or trace[3][0] != "scan_req"
        ):
            print(f"WARN: Trace {index} does not start with pre {trace}!")
            assert False, f"WARN: Trace {index} does not start with pre {trace}!"
        else:
            # last response is BTLE_ADV_IND which is still part of pre (reset)
            # so we can replace it with Empty to ignore the connecting (part of pre)
            new_trace = [empty] + trace[4:]
            assert len(new_trace) > 1, f"Trace too short {index}, {new_trace}"
            new_traces.append(new_trace)

    traces = new_traces
    input_alphabet = set(el[0] for trace in traces for el in trace[1:])
    output_alphabet = set(el[1] for trace in traces for el in trace[1:])
    print("traces", len(traces))
    print("input alphabet", len(input_alphabet), input_alphabet)
    print("output alphabet", len(output_alphabet), output_alphabet)

    startouts = set(trace[0] for trace in traces)
    if len(startouts) > 1:
        print("WARN: we have multiple starting outputs (not same starting state)")
        print(startouts)
        assert False

    with open("nRF52832_moore_without_mtu_req_parsed_trace_cleaned.json", "w") as f:
        json.dump(traces, f)


if __name__ == "__main__":
    exit(main())
