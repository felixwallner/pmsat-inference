import argparse
import copy
import itertools as it
import json
import queue
import random
import sys

from aalpy.automata import MealyState, MooreMachine, MooreState
from aalpy.base import SUL, AutomatonState, DeterministicAutomaton, Oracle
from aalpy.learning_algs import run_Lstar
from aalpy.oracles import StatePrefixEqOracle, WMethodEqOracle
from aalpy.SULs import MooreSUL
from aalpy.utils import load_automaton_from_file


class PerfectEqOracle(Oracle):
    def __init__(self, alphabet: list, sul: SUL, mm: MooreMachine):
        super().__init__(alphabet, sul)
        self.mm = mm

    def find_cex(self, hypothesis):
        """
        Return a counterexample (inputs) that displays different behavior on system under learning and
        current hypothesis.

        Args:

          hypothesis: current hypothesis

        Returns:

            tuple or list containing counterexample inputs, None if no counterexample is found
        """
        self.reset_hyp_and_sul(hypothesis)
        # print(hypothesis)
        mm = self.mm  # mm: MooreMachine = copy.deepcopy(self.mm)
        assert set(mm.get_input_alphabet()) == set(
            self.alphabet
        ), f"{mm.get_input_alphabet()} != {self.alphabet}"
        dis = mm.find_distinguishing_seq(mm.current_state, hypothesis.current_state)
        if dis is None:
            return None  # no CEX found
        # print("Found cex:", dis)
        # return dis
        assert self.sul.step(None) == hypothesis.step(None)
        for index, inp in enumerate(dis):
            out1 = self.sul.step(inp)
            out2 = hypothesis.step(inp)
            if out1 != out2:
                assert (
                    index == len(dis) - 1
                ), f"Difference in output not on last index? {index} != {len(dis)-1}"
                return dis
                # print("Found difference in output without exception??")
        assert False, "Did not find difference in output on performing CEX??"


class TracedMooreSUL(MooreSUL):
    def __init__(self, mm: MooreMachine) -> None:
        super().__init__(mm)
        self.current_trace = [self.mm.step(None)]
        self.traces = [self.current_trace]

    def pre(self):
        super().pre()
        assert self.mm.current_state == self.mm.initial_state
        if len(self.current_trace) > 1:
            new_trace = [self.mm.step(None)]
            self.traces.append(new_trace)
            self.current_trace = new_trace

    def step(self, input, description: str | None = None):
        output = super().step(input)
        if input is None:
            return output
        if description is not None:
            entry = (input, output, description)
        else:
            entry = (input, output)
        self.traces[-1].append(entry)
        return output


class SingleTraceMooreSUL(TracedMooreSUL):
    def pre(self) -> None:
        """Reset with steps (only perfect knowledge allows for shortest path)"""
        path = self.mm.get_shortest_path(self.mm.current_state, self.mm.initial_state)
        for letter in path:
            self.step(letter)
        assert self.mm.current_state == self.mm.initial_state


def bisimilar(a1: DeterministicAutomaton, a2: DeterministicAutomaton):
    """
    Checks whether the provided moore machines are bisimilar
    """

    to_check = queue.Queue[tuple[AutomatonState, AutomatonState]]()
    to_check.put((a1.initial_state, a2.initial_state))
    requirements = dict()
    requirements[(a1.initial_state, a2.initial_state)] = []

    while not to_check.empty():
        s1, s2 = to_check.get()

        if (isinstance(s1, MooreState) and s1.output != s2.output) or (
            isinstance(s1, MealyState) and s1.output_fun != s2.output_fun
        ):
            return requirements[(s1, s2)]

        t1, t2 = s1.transitions, s2.transitions
        for t in it.chain(t1.keys(), t2.keys()):
            if (t in t1.keys()) != (t in t2.keys()):
                return requirements[(s1, s2)] + [t]

        for t in t1.keys():
            c1, c2 = t1[t], t2[t]
            if (c1, c2) not in requirements:
                requirements[(c1, c2)] = requirements[(s1, s2)] + [t]
                to_check.put((c1, c2))


def parse_args(cmd_args):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "moore_dot_file", type=str, help="Automata to learn and generated trace from"
    )
    args = parser.parse_args(cmd_args)
    return args


def steps_traces(traces: list[list]) -> int:
    return sum(len(trace) - 1 for trace in traces)


def main(cmd_args: list[str] | None = None):
    args = parse_args(sys.argv[1:] if cmd_args is None else cmd_args)
    file = args.moore_dot_file
    mm: MooreMachine = load_automaton_from_file(file, "moore", True)
    inputs = sorted(set(mm.get_input_alphabet()))
    outputs = sorted(set(state.output for state in mm.states))
    assert mm.is_minimal(), "Machine was not minimal!"

    use_single_trace = False

    if use_single_trace:
        sul = SingleTraceMooreSUL(mm)
        assert mm.is_strongly_connected(), "Automaton must be strongly connected for single trace"
    else:
        sul = TracedMooreSUL(mm)

    random.seed(42)

    oracle = WMethodEqOracle(inputs, sul, 14, False)
    cex = oracle.find_cex(copy.deepcopy(mm))
    assert cex is None, "Found CEX on identical machine...??"
    traces = sul.traces
    sul.traces = []
    print(f"Total {len(traces)} traces with total steps {steps_traces(traces)}")

    # fill queries to length 10 with random inputs
    for trace in traces:
        mm.reset_to_initial()
        sul.pre()
        for index, (i, o) in enumerate(trace[1:]):
            assert sul.step(i) == o
        for _ in range(index + 1, 10):
            sul.step(random.choice(inputs))
        sul.post()

    traces = sul.traces

    print(f"Total {len(traces)} traces with total steps {steps_traces(traces)}")

    input_seq = [[i for i, _ in trace[1:]] for trace in traces]
    with open(file[:-4] + "_input_sequences.txt", "w") as f:
        json.dump(input_seq, f)


if __name__ == "__main__":
    main()
