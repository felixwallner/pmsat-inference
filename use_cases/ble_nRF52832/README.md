# nRF52832 Experiment

* `nRF52832_original.dot` was taken from <https://github.com/apferscher/ble-learning>, specifically commit `7791d41` from <https://github.com/apferscher/ble-learning/blob/main/learned-automata/nRF52832.dot> on 14.03.2023.
* `nRF52832_moore_original.dot` was generated from `nRF52832_original.dot` using script `mealy_to_moore.py`
* The input `mtu_req` was removed from model using script `only_inputs.py` resulting in `nRF52832_moore_without_mtu_req.dot`
* which was used to generate a trace using the W-method and random inputs with `generate_ble_traces.py` which resulted in the input sequence `nRF52832_moore_without_mtu_req_input_sequences.txt`
* Said input sequences were replayed on the actual nRF52832 device and its output recorded in `nRF52832_moore_without_mtu_req_input_sequences_device_output.txt`
* The device output was parsed to the final device traces in `parse_ble_output_to_trace.py` generating `nRF52832_moore_without_mtu_req_parsed_trace_cleaned.json`
* which were finally used during learning with `run_pmsat_on_traces.py`