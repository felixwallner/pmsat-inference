# Partial Max-SAT Inference Algorithm and Publication Artifacts

This [repository](#artifacts) contains the source code of the implementation used for the [PMSAT inference algorithm](#algorithm).
It also contains the scripts used to generate benchmark sets and run the uses cases presented in the paper "[It's Not a Feature, It's a Bug: Fault-Tolerant Model Mining from Noisy Data](#paper)" by [Felix Wallner](https://orcid.org/0009-0004-8129-9928), [Bernhard Aichernig](https://orcid.org/0000-0002-3484-5584) and [Christian Burghard](https://orcid.org/0009-0009-3246-4399) accepted to the [ICSE 2024 Conference](https://conf.researchr.org/home/icse-2024).

## Table of Contents

- [Partial Max-SAT Inference Algorithm and Publication Artifacts](#partial-max-sat-inference-algorithm-and-publication-artifacts)
  - [Table of Contents](#table-of-contents)
- [Purpose](#purpose)
  - [PMSAT-Inference Algorithm](#pmsat-inference-algorithm)
  - [Benchmarking Sets](#benchmarking-sets)
  - [Use Case Studies](#use-case-studies)
  - [Examples](#examples)
  - [Scripts](#scripts)
- [Provenance](#provenance)
  - [Paper](#paper)
  - [Artifacts](#artifacts)
- [Data](#data)
- [Setup](#setup)
  - [Hardware](#hardware)
  - [Operating Systems](#operating-systems)
  - [Requirements](#requirements)
  - [Building](#building)
- [Usage](#usage)
  - [Algorithm](#algorithm)
  - [Recreating Figures and Tables](#recreating-figures-and-tables)
    - [Figure 1](#figure-1)
    - [Figure/Table 2](#figuretable-2)
      - [Trace Generation](#trace-generation)
    - [Figure/Table 3](#figuretable-3)
      - [Table 3 + Figure 3a, 3b and 3c](#table-3--figure-3a-3b-and-3c)
      - [Figure 3d](#figure-3d)
      - [Figure 3e](#figure-3e)
      - [Figure 3f](#figure-3f)
    - [Table 4](#table-4)
    - [Table 5](#table-5)
    - [Table 6](#table-6)
    - [Benchmarks](#benchmarks)

---

# Purpose

The [artifacts](#artifacts) present in our [paper](#paper) published in the research track of ICSE 2024 are composed of the [PMSAT-Inference algorithm](#pmsat-inference-algorithm), [benchmarking sets](#benchmarking-sets), [use case studies](#use-case-studies), [examples](#examples) and a diverse set of [scripts and wrappers](#scripts) to produce the models and analyze the results.

The [repository](#artifacts) allows for the usage of the algorithm for model mining from noisy execution traces, in code or directly with our scripts. Additionally, it also contains *all* information/data required to verify the results presented in our paper.

We applied for the following badges:

* **Artifacts Available**, due to the algorithm and *all* used results being openly and freely available (see [provenance](#artifacts)).
* **Artifacts Reusable**, due to the extensive documentation about how to use our algorithm and how each result presented in the paper can be *reproduced* using the scripts provided (see [usage](#usage)). We also added a Docker setup to allow for exact reproduction of our environment and use by others (see [setup](#setup)).

In more detail, the artifacts are composed of the following parts:

## PMSAT-Inference Algorithm

The implementation of our model mining algorithm is the most important artifact. This algorithm can infer behavioral models from (execution) traces, even in the presence of noise or faults in the data. It is explained in detail in Section 3 of our paper.

Our Python implementation can be found in the folder [pmsatlearn](pmsatlearn). This was the implementation used for all evaluations in the paper. We especially prepared it in such a way that it can [easily be used](#usage) for other traces and integrating it with the automata learning library [AALpy](https://github.com/DES-Lab/AALpy) for further re-usability.

All other artifacts either make use of this algorithm or analyze/contain its results.

## Benchmarking Sets

A number of generated benchmarking sets and the results of using our algorithm on them to evaluate its performance and generate plots for the paper.

The randomly generated original models as well as the inference results can be found in the folders `benchmarkingset-*`. (We explain each folder and how to reproduce it [here](#benchmarks))

## Use Case Studies

The original data and results of three use case studies of real devices that were evaluated in the paper.

The traces, and in the case of the BLE device also the scripts to generate them, as well as the learning results can be found in the folder [use_cases](use_cases).

## Examples

Throughout the paper we use a number of examples for which we present results and figures. These can be found in the folder [examples-results](examples-results).

## Scripts

Finally, a large number of helper scripts and wrappers for the generation, learning and evaluation of models/results are present. We explain in detail how and for what each of these should be used below.


---

# Provenance

## Paper

The paper "It's Not a Feature, It's a Bug: Fault-Tolerant Model Mining from Noisy Data" was published Open Access in the research track of [ICSE 2024](https://conf.researchr.org/home/icse-2024) and is - at this time - freely available for download in the [ACM Digital Library](https://www.computer.org/csdl/proceedings-article/icse/2024/021700a316/1RLIWucW0SY).

Later, the paper will be registered to its [DOI](https://doi.org/10.1145/3597503.3623346) in case the above link should be changed in the future.

## Artifacts

The repository containing the algorithm, benchmarking sets, use cases, results and scripts for the evaluated artifact submission can be found at Zenodo: [doi.org/10.5281/zenodo.10423670](https://doi.org/10.5281/zenodo.10423670)


> Additionally, the PMSAT inference algorithm is actively maintained on [gitlab.com/felixwallner/pmsat-inference](https://gitlab.com/felixwallner/pmsat-inference)

---

# Data

This section lists all data and sources that cannot easily be reproduced or were taken from other sources:

In the case of our paper, the only data that is not easily reproducible are original traces of our [use cases](use_cases).  For these three we observed the behavior of real devices. Not only that, but all three devices exhibit random faults such as message loss or time triggered transitions, such that reproduction of the *exact* traces is infeasible.

Traces that came from observing real device behavior are:

* `use_cases/avl_APC/apc_trace.json`: [Advanced Particle Counter (APC)](#table-4)
* `use_cases/avl415SE_smokemeter/smokemeter_trace.json`: [AVL 415 SE SmokeMeter (SmokeMeter)](#table5)
* `use_cases/ble_nRF52832/nRF52832_moore_without_mtu_req_parsed_trace_cleaned.json`: [Bluetooth Low Energy  nRF52832 (BLE)](#table-6)
  

Additionally, for the BLE device, we first had to generate input queries that we could then replay on the BLE chip. For this we downloaded the `use_cases/ble_nRF52832/nRF52832_original.dot` from the [ble learning repository](https://github.com/apferscher/ble-learning), specifically commit `7791d41` from [here](https://github.com/apferscher/ble-learning/blob/main/learned-automata/nRF52832.dot), on 14.03.2023. Replaying the generated queries on the real device then resulted in `use_cases/ble_nRF52832/nRF52832_moore_without_mtu_req_parsed_trace_cleaned.json`, which we used for model mining. (For the exact steps taken see [note](use_cases/ble_nRF52832/README.md))
For more details on the learning of BLE devices please see the original paper by Andrea Pferscher, [Fingerprinting Bluetooth Low Energy Devices via Active Automata Learning](https://doi.org/10.1007/978-3-030-90870-6_28), and the corresponding [repository](https://github.com/apferscher/ble-learning).


All other data was generated randomly by us using scripts and can be reproduced. For the exact reproduction see [setup](#setup) followed by [usage](#recreating-figures-and-tables).

---

# Setup

This section explains the steps necessary to use our algorithm and re-create the artifacts presented in our paper:

*If you want to re-create our artifacts*, then we recommend to use the **Docker** setup for recreating the environment as exactly as possible.

*If you only want to use the algorithm*, you may also **manually install** the requirements on your system: use Python 3.10 or newer, a virtual environment (e.g. `python -m venv venv`) and install the requirements with `pip install -r requirements.txt`. You are also going to need to install `graphviz` for visualization. In this case skip directly to [usage](#algorithm).

In any case, you will need to download or clone the [repository](#artifacts) from Zenodo or Gitlab respectively.

## Hardware

There are no special hardware requirements.
The original results were achieved on an *AMD Ryzen 9 5950X CPU*, in general, the better the single threaded performance of the CPU, the better.

> It should be noted that we included a timeout of one hour for most of our experiments. These results would change w.r.t. differences in performance and potentially other usage and workload of the used device.

Additionally, although the original system had 32 GB of RAM, this amount should not be required to recreate the results. However, it might be necessary to reduce the number of concurrently running benchmarks if the amount of RAM is much lower, which would result in slower benchmarking overall.
For all but benchmarking less RAM should not make a difference.

## Operating Systems

We tested the Docker setup natively on **Ubuntu 22.04** and on **WSL** (Windows Subsystem for Linux) on Windows 10.

> In case you are running with WSL on Windows, the root folder of your WSL file system can be accessed via `\\wsl.localhost\[your WSL installation]\home\[your username]`. There you should put (or clone) the `pmsat-inference` folder (named exactly like this!). This folder will be available as a *Docker Volume* for access, e.g. for inspecting pdf files. For example, the final path on our WSL was `\\wsl.localhost\Ubuntu-18.04\home\[username]\pmsat-inference`

> The setup should also work for Docker Desktop (on Windows), although it was not explicitly tested there.

## Requirements

Install **Docker** and **docker-compose** on your machine using [this guide](https://docs.docker.com/compose/install/) and make sure the daemon is running (e.g. by running `docker ps`).

> On Ubuntu 22.04 both can simply be installed with `sudo apt -y install docker.io docker-compose`

## Building

> We assume that your user has permission to run docker commands directly. If not - on Linux or in WSL - prepend `sudo` to the docker and docker-compose commands.

In the top folder of this [repository](#artifacts), aka.`pmsat-inference`, run the following command to build the docker container:

```bash
docker-compose build
```

> This process takes longer the first time it is run and will be almost instantaneous on subsequent builds

Then, run the following to start the container in interactive mode:

```bash
docker-compose run pmsat
```

This should give you a terminal with access to the prepared environment and files required for recreating the artifacts.
(E.g. type `pip list` to see the installed requirements such as `aalpy` among others; or type `ls` to see the files such as `run_pmsat_on_traces.py` among others)

> Note: All further commands below are executed in this docker container!

---

# Usage

## Algorithm

The source code of the actual inference algorithm lives in the folder [pmsatlearn](pmsatlearn). It can be used as follows in a Python interpreter or from a Python script:

```python
from pmsatlearn import run_pmSATLearn

traces = [
    ["offline", ("disconnect", "offline"), ("connect", "online")],
    ["offline", ("connect", "online"), ("connect", "online"), ("disconnect", "offline")],
    ["offline", ("disconnect", "offline"), ("connect", "offline")],
]
n = 2 # the number of states of the inferred automaton
pmstrategy = "rc2" # can be "lsu", "fm" or "rc2"

deterministic_moore, stochastic_moore, info = run_pmSATLearn(
    traces,
    n,
    "moore",
    pm_strategy=pmstrategy,
    timeout=None,
    print_info=True,
)

deterministic_moore.visualize("deterministic")
stochastic_moore.visualize("stochastic") # glitched transitions have the "!" prefix
```

Additionally, the algorithm can also be used from the helper scripts directly:
To run PMSAT inference on a set of traces in JSON format, providing either `n` for a specific number of states or `nmax` to infer all automata up to that given number of states, use the following:

```bash
python run_pmsat_on_traces.py [TRACE_FILE.json] -nmax 15
```

Alternatively, you may load a Moore machine in `.dot` format, then generate traces on the fly and inject glitches by dropping a certain percentage of steps from the traces:

```bash
python run_pmsat_on_moore.py [MOORE_MACHINE.dot]
```

Both support a number of parameters to customize the run and generation of traces respectively (use `-h` flag with the scripts to display all options).

## Recreating Figures and Tables

This section describes in detail the necessary steps and commands to reproduce all tables and figures used in our paper. We took great care to ensure reproducibility by setting random seeds, not using sets or sorting results were possible. However, there are cases were the *results may differ slightly* in the following ways:

Firstly, whenever our algorithm is used, e.g. when `run_pmsat_on_traces.py` is invoked, the underlying SAT solver may potentially gives us *one of multiple equally good solutions* (if they exist). This is because the SAT solver seems to internally use sets (where the order is different for each run) and may find one solution before another depending on that order. 
This happens, for example, with [figure 2](#figuretable-2), where there are multiple different solutions for n=5 with 4 glitches. Depending on the exact SAT solution the glitched transitions may differ slightly.
This in turn subtly influences some of the numbers in result tables, such as the mean glitches delta frequency, and the resulting stochastic automata figures.

Lastly, most of our experiments were performed with a *one hour timeout*. That means that differences in single threaded performance, heavy usage of the device or other factors may cause *differences in average solving time and percentage of correctly inferred models* (we included timeouts as incorrectly inferred models), such as in [figures 3a, 3b and 3c](#table-3--figure-3a-3b-and-3c).
This would be relevant if the [benchmarks](#benchmarks) were run again.

> Please note that it can take multiple hours or days to generate and infer some or all of the benchmarks!

> Note that every folder of experiments contains an `info.json` file. This file contains all the settings and details on how the results in this folder were generated, such as the traces, glitched traces (if applicable), parameters and more.

### Figure 1

Run the following commands in order:

```bash
# mine model from simple server example traces (without glitches)
python run_pmsat_on_example.py server

# mine models from simple server example traces with glitch
python run_pmsat_on_example.py server_glitch

# visualize automata (dot -> pdf)
python compile_dot_to_pdf.py EXAMPLE-results
```

The first two lines run the algorithm on the **small server example**, first without and then with glitches, the results of which are saved to `EXAMPLE-results/a406d7766069b85cae0354e68ee5456a` and `EXAMPLE-results/3c20b2e8b950002550ddcd0652ee186b` respectively.
(The hashes are the hashed traces)

The last line then compiles the `.dot` files (containing graphical representations of automata) using **graphviz** to `.pdf` files for visual inspection.

**Results:**

* Figure 1a (without glitch): `EXAMPLE-results/a406d7766069b85cae0354e68ee5456a/pmsatLearned-rc2-N2-RN2.pdf`
* Figure 1a (learned det.): `EXAMPLE-results/3c20b2e8b950002550ddcd0652ee186b/pmsatLearned-rc2-N2-RN2.pdf`
* Figure 1b: `EXAMPLE-results/3c20b2e8b950002550ddcd0652ee186b/pmsatLearned-rc2-N2-RN2-STOC.pdf`

### Figure/Table 2

Run the following commands in order:

```bash
# mine models from ping pong example traces
python run_pmsat_on_traces.py examples-results/ping_pong_example/info.json -nmax 7

# read from results and pipe Table 2 to file
python parse_single_run_results.py TRACE-results/92e710ef352c4739cd7569794272588a > table2.csv

# visualize automata (dot -> pdf)
python compile_dot_to_pdf.py TRACE-results/92e710ef352c4739cd7569794272588a
```

This command runs the algorithm on the traces of the **ping pong example** used in the paper, the results of which are saved to `TRACE-results/92e710ef352c4739cd7569794272588a`.

**Results:**

* Figure 2a: `TRACE-results/92e710ef352c4739cd7569794272588a/pmsatLearned-rc2-N3-STOC.pdf`
* Figure 2b: `TRACE-results/92e710ef352c4739cd7569794272588a/pmsatLearned-rc2-N4-STOC.pdf`
* Figure 2c: `TRACE-results/92e710ef352c4739cd7569794272588a/pmsatLearned-rc2-N5-STOC.pdf`
* Table 2: `table2.csv` (with additional columns not used in the paper)

#### Trace Generation

Originally, the traces present in `examples-results/ping_pong_example/info.json` were randomly generated.
To recreate the same traces run the following commands:

```bash
# generate model for ping pong example (-> example_ping_pong.dot)
python run_pmsat_on_example.py ping_pong

# generate traces with glitches from model
python run_pmsat_on_moore.py example_ping_pong.dot -generation_method lstar -steps 500 -walk_len 5 -glitch_percent 0.01 -seed 65 --only_generate

# mine models from newly generated ping pong example traces
python run_pmsat_on_traces.py MOORE-results/92e710ef352c4739cd7569794272588a/info.json -nmax 7
```

> Note: Observe that the newly generated traces have the same hash (and will be saved to the same location) as the traces in `examples-results/ping_pong_example/info.json`

You may also generate new/different traces with a wide variety of customizeable parameters (see `python run_pmsat_on_moore.py -h`)

**Intermediate Results:**

* Traces for ping pong example: `MOORE-results/92e710ef352c4739cd7569794272588a/info.json` (equivalent to `examples-results/ping_pong_example/info.json`)

### Figure/Table 3

#### Table 3 + Figure 3a, 3b and 3c

These three figures and the table were all generated by analyzing the results of our largest benchmarking set with a total of 5000 experiments: `benchmarkingset-rc2-results`.
The benchmarks differ in the **number of states**, **maximum number of outputs** and **number of discarded steps** in traces (i.e. injected glitches).

The following commands generate the respective table/figures:

```bash
# count the number of timeouts and pipe to file (-> table3.csv)
python parse_all_results_timeouts.py benchmarkingset-rc2-results > table3.csv

# generate plot for % solved, plotting max outputs (-> figure3a.png)
python parse_all_results_solve_out.py benchmarkingset-rc2-results

# generate plot for % solved, plotting percentage of glitches (-> figure3b.png)
python parse_all_results_solve_drop.py benchmarkingset-rc2-results

# generate plot for avg. solving time, plotting percentage of glitches (-> figure3c.png)
python parse_all_results_time_drop.py benchmarkingset-rc2-results
```

**Results:**

* Table 3: `table3.csv`
* Figure 3a: `figure3a.png`
* Figure 3b: `figure3b.png`
* Figure 3c: `figure3c.png`

#### Figure 3d

The benchmarking set `benchmarkingset-faults-results` contains the benchmarks and results of injecting **different types of faults** into the traces, instead of only dropping input-output pairs.

The results can be analyzed with the following command, which will generate the corresponding figure:

```bash
# generate plot for different faults (-> figure3d.png)
python parse_all_results_faults.py benchmarkingset-faults-results
```

**Results:**

* Figure 3d: `figure3d.png`

#### Figure 3e

The benchmarking set `benchmarkingset-trace-results` contains the benchmarks and results of solving for  **traces of different length**.

The analysis of the trace length benchmarks, including drawing the box plot, can be done with:

```bash
# generate plot for different trace lengths (-> figure3e.png)
python parse_all_results_trace.py benchmarkingset-trace-results
```

**Results:**

* Figure 3e: `figure3e.png`

#### Figure 3f

The benchmarking set `benchmarkingset-o5-g1` contains random automata and traces for |O| <= 5 and 1% injected glitches. We use this to compare our algorithms performance against the **performance of IO ALERGIA**.

With the following command we *run* IO ALERGIA against each example in the benchmarking set (with different parameters) and then generate a plot displaying the relative [F1-scores](https://en.wikipedia.org/wiki/F-score):

```bash
# run IO ALERGIA and generate plot (-> figure3f.png)
python evaluate_results_alergia_fscore.py benchmarkingset-o5-g1
```

> Note, this may take a few minutes to run as IO ALERGIA is performed 6 times per example

**Results:**

* Figure 3f: `figure3f.png`

### Table 4

In the first use case, the AVL **Advanced Particle Counter (APC)**, we aquired the trace in `use_cases/avl_APC/apc_trace.json` through observation and instrumentation of a real APC device (see [data](#data)).

The model mining from said trace and analyzing of the result can be done using the following commands:

```bash
# mine models from use case APC trace
python run_pmsat_on_traces.py use_cases/avl_APC/apc_trace.json -nmax 13

# read from results and pipe Table 4 to file
python parse_single_run_results.py TRACE-results/cec0073ace4949b9131532f72e03f409 > table4.csv

# visualize automata (dot -> pdf)
python compile_dot_to_pdf.py TRACE-results/cec0073ace4949b9131532f72e03f409
```

**Results:**

* Table 4: `table4.csv` (with additional columns not used in the paper)

### Table 5

Similarly to the use case before, a real **AVL 415 SE SmokeMeter (SmokeMeter)** device was observed and instrumented resulting in the trace `use_cases/avl415SE_smokemeter/smokemeter_trace.json`.

The model mining from said trace and analyzing of the result can be done using the following commands:

```bash
# mine models from use case SmokeMeter trace
python run_pmsat_on_traces.py use_cases/avl415SE_smokemeter/smokemeter_trace.json -nmax 14

# read from results and pipe Table 5 to file
python parse_single_run_results.py TRACE-results/2804424892661995c1e6a665fa35e490 > table5.csv

# visualize automata (dot -> pdf)
python compile_dot_to_pdf.py TRACE-results/2804424892661995c1e6a665fa35e490
```

**Results:**

* Table 5: `table5.csv` (with additional columns not used in the paper)

### Table 6

Finally, our last use case was the **Bluetooth Low Energy  nRF52832 (BLE)** device, which resulted in `use_cases/ble_nRF52832/nRF52832_moore_without_mtu_req_parsed_trace_cleaned.json` after we replayed our generated input queries on the device. 
A number of steps were required to allow us to use our generated queries and process the trace (see [data](#data) for more details).

The model mining from said trace and analyzing of the result can be done using the following commands:

```bash
# mine models from use case BLE traces
python run_pmsat_on_traces.py use_cases/ble_nRF52832/nRF52832_moore_without_mtu_req_parsed_trace_cleaned.json -nmax 16

# read from results and pipe Table 6 to file
python parse_single_run_results.py TRACE-results/bdec710c6bfabb38f70d9dc5a452f8c6 > table6.csv

# visualize automata (dot -> pdf)
python compile_dot_to_pdf.py TRACE-results/bdec710c6bfabb38f70d9dc5a452f8c6
```

**Results:**

* Table 6: `table6.csv` (with additional columns not used in the paper)


### Benchmarks

All of our benchmarks were generated using the following procedure:

1. Select a number of parameters for generation and inferring
2. Generate a random Moore machine using AALpy and the given parameters
3. Query the generated Moore machine and record the observations as traces
4. Inject faults into the traces, e.g., by dropping steps
5. Use PMSAT inference to learn from the noisy traces and
6. Save results and stats, such as solve time, correctness and more

> Multiple benchmarks are run at the same time using multi-threading, so running the benchmarks with more threads is recommended.

All of the benchmarks and the results of inferring them can be found in the folders `benchmarkingset-*`.
Here is a short description what each benchmarking set was used for:

* `benchmarkingset-lsu-fm-rc2-results` contains the 500 experiments each used by the three different PMSAT algorithm. The performance difference was remarked upon in the paper and RC2 was selected for all other benchmarks due to it performing best in this benchmarking set.
* `benchmarkingset-rc2-results` contains the larger benchmarking set only for the RC2 PMSAT algorithm (our default) for performance analysis.
* `benchmarkingset-no-optimisation-results` contains the identical benchmarking set as RC2 (5000 experiments) run with **no optimisation** (without optimization Equation 8). This was used for a single sentence in the paper about how much the optimization increases performance.
* `benchmarkingset-trace-results` contains the benchmarks with extended traces in order to analyses performance w.r.t. trace steps.
* `benchmarkingset-faults-results` contains the benchmarks with different fault types in order to analyses performance w.r.t. fault type.
* `benchmarkingset-o5-g1` contains the benchmarks for the IO ALERGIA comparison in Figure 3e.

> Please note that it can take multiple hours or days to generate and infer some or all of the benchmarks!

The benchmarking sets can be generated with the files `benchmark*.py` files. Please note that these files do not take command line arguments and parameters may have to be changed inside the files before running them.

> Before running please rename the target folders or change the output paths inside the `benchmark*.py` files to not overwrite (parts of) the existing results!

The benchmarks can be recreated and inferred in the following ways:

* `benchmarkingset-trace-results` can be produced by running `benchmark_trace.py`
* `benchmarkingset-faults-results` can be produced by running `benchmark_faults.py`
* `benchmarkingset-rc2-results` can be produced by running the unchanged `benchmark.py` (which will save the results to the folder `BENCHMARKS-results`)
* `benchmarkingset-no-optimisation-results` can be produced by running the unchanged `benchmark.py` but remove line 110 from `pmsatlearn/learnalgo.py` to remove the optimization
* `benchmarkingset-lsu-fm-rc2-results` can be produced by adapting the lines 285, 294 and 295 in `benchmark.py`, where the seed is fixed to only 43 and the different algorithms are enabled
* `benchmarkingset-o5-g1` can be produced by adapting line 384 in `benchmark_faults.py` using only the 'discard' option


---

