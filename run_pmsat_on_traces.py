"""Take a given set of traces from a json file
(expected format a list of lists, where each inner list is a Moore trace, 
i.e., a trace starting with an output followed by input, output pairs)
and learn the given traces using pmsatlearn."""

import argparse
import hashlib
import itertools as it
import json
import multiprocessing
import queue
import sys
from pathlib import Path

import pebble
from aalpy.automata import MealyState, MooreState
from aalpy.base import AutomatonState, DeterministicAutomaton
from aalpy.learning_algs import run_Alergia

from pmsatlearn import run_pmSATLearn


def steps_traces(traces: list[list]) -> int:
    return sum(len(trace) - 1 for trace in traces)


def bisimilar(a1: DeterministicAutomaton, a2: DeterministicAutomaton):
    """
    Checks whether the provided moore machines are bisimilar
    """

    to_check = queue.Queue[tuple[AutomatonState, AutomatonState]]()
    to_check.put((a1.initial_state, a2.initial_state))
    requirements = dict()
    requirements[(a1.initial_state, a2.initial_state)] = []

    while not to_check.empty():
        s1, s2 = to_check.get()

        if (isinstance(s1, MooreState) and s1.output != s2.output) or (
            isinstance(s1, MealyState) and s1.output_fun != s2.output_fun
        ):
            return requirements[(s1, s2)]

        t1, t2 = s1.transitions, s2.transitions
        for t in it.chain(t1.keys(), t2.keys()):
            if (t in t1.keys()) != (t in t2.keys()):
                return requirements[(s1, s2)] + [t]

        for t in t1.keys():
            c1, c2 = t1[t], t2[t]
            if (c1, c2) not in requirements:
                requirements[(c1, c2)] = requirements[(s1, s2)] + [t]
                to_check.put((c1, c2))


def parse_args(cmd_args):
    parser = argparse.ArgumentParser()
    parser.add_argument("trace_json_file", type=Path, help="Traces to learn from")
    parser.add_argument(
        "-nmax", type=int, default=None, help="Use up to nmax number of states"
    )
    parser.add_argument("-n", type=int, default=None, help="Learn for exactly n states")
    parser.add_argument(
        "-pmstrat",
        choices=["rc2", "fm", "lsu"],
        default="rc2",
        help="PMSAT implementation",
    )
    parser.add_argument(
        "-timeout", type=int, default=None, help="Timeout for individual learning run"
    )
    parser.add_argument(
        "-max-workers",
        type=int,
        default=None,
        help="Number of workers to run in parallel",
    )
    parser.add_argument(
        "-max-tasks",
        type=int,
        default=0,
        help="Number of tasks/calculations to run in parallel",
    )
    args = parser.parse_args(cmd_args)
    return args


def main(cmd_args: list[str] | None = None):
    args = parse_args(sys.argv[1:] if cmd_args is None else cmd_args)
    file = args.trace_json_file
    max_tasks = args.max_tasks
    max_workers = args.max_workers
    pmstrat = args.pmstrat
    n = args.n
    print_info = args.n is not None
    nmax = args.nmax
    timeout = args.timeout
    if timeout is not None and timeout <= 0:
        timeout = None
    if n is None and nmax is None:
        print("Choose either n or nmax")
        exit(1)
    if n is not None and nmax is not None:
        print("Only use either n or nmax as arguments!")
        exit(1)

    with file.open("r") as f:
        traces = json.load(f)
    if type(traces) == dict:
        traces = traces["traces"]
    assert type(traces) == list and all(type(trace) == list for trace in traces)
    traces = [[trace[0]] + [tuple(t) for t in trace[1:]] for trace in traces]

    ## prepare traces and visualization

    traces_hash = hashlib.md5(
        str(traces).encode("utf8"), usedforsecurity=False
    ).hexdigest()
    path = Path("TRACE-results") / traces_hash
    # path = Path("TRACE-results") / Path(file.name + "-results")
    path.mkdir(parents=True, exist_ok=True)

    with (path / "info.json").open("w") as f:
        json.dump(
            {
                "original_automaton": file.as_posix(),
                "traces": traces,
            },
            f,
        )

    print(f"{len(traces)} traces of total length: {steps_traces(traces)}")

    ## use simple trace for IO ALERGIA

    simple_moore_traces = [
        ([trace[0]] + [tuple(entry[:2]) for entry in trace[1:]])
        for trace in traces
        if len(trace) > 1
    ]

    # ## Learn with IO ALERGIA
    try:
        alergia_learned = run_Alergia(
            simple_moore_traces, "mdp", print_info=True, eps=0.5
        )
        alergia_learned.save((path / "alergiaLearned").as_posix())
    except ZeroDivisionError:
        print("WARN: Alergia could not calculate probabilities")
    outputs = sorted(
        set(trace[0] for trace in traces)
        | set(o for trace in traces for _, o in trace[1:])
    )
    if n is not None:
        nmax = n
        nmin = n
    else:
        assert nmax is not None
        nmin = len(outputs)

    if nmax < nmin:
        print("WARN: nmax is smalled than the size of the output alphabet!")
        print("Output alphabet:", outputs)
        exit(1)

    if max_workers is None:
        max_workers = min(multiprocessing.cpu_count(), len(range(nmin, nmax + 1)))
    futures = []
    print(
        f"Starting {len(range(nmin, nmax + 1))} calcaulations on {max_workers} workers..."
    )
    with pebble.ProcessPool(max_workers=max_workers, max_tasks=max_tasks) as pool:
        for n in range(nmin, nmax + 1):
            future = pool.schedule(
                run_pmSATLearn,
                (
                    traces,
                    n,
                ),
                {
                    "automata_type": "moore",
                    "pm_strategy": pmstrat,
                    "timeout": timeout,
                    "print_info": print_info,
                },
            )
            futures.append((future, n))

        result_det_pmsat_automaton = [None] * (nmax + 1)
        for future, n in futures:
            pmsat_learned, pmsat_learned_stoc, info = future.result()
            print(f"RESULTS for {n} states:")
            if pmsat_learned is not None:
                name = f"pmsatLearned-{pmstrat}-N{n}"
                print("#glitches:", len(info["glitch_steps"]))
                result_det_pmsat_automaton[n] = pmsat_learned
                assert pmsat_learned_stoc is not None
                pmsat_learned.save((path / name).as_posix())
                stoc_name = (path / (name + "-STOC")).as_posix()
                pmsat_learned_stoc.save(stoc_name)
                try:
                    with open(stoc_name + ".dot", "r") as f:
                        dot_stoc_automaton = f.read()
                    with open(stoc_name + ".dot", "w") as f:
                        for i in range(1, 1000):
                            if f'] t{i}"];' not in dot_stoc_automaton:
                                break
                            dot_stoc_automaton = dot_stoc_automaton.replace(
                                f'] t{i}"];', ']"];'
                            )
                        f.write(
                            dot_stoc_automaton.replace('label="!', 'color=red, label="')
                        )
                except FileNotFoundError:
                    print("WARN: Could not find file for stochastic edge coloring red")

                for automaton_index, learned_pmsat_automaton in enumerate(
                    result_det_pmsat_automaton
                ):
                    if learned_pmsat_automaton is not None:
                        if bisimilar(pmsat_learned, learned_pmsat_automaton) is None:
                            info["bisimilar_to_pmsat_with_states"] = automaton_index
                            break
                assert info["bisimilar_to_pmsat_with_states"]
            else:
                print("PMSAT - UNSAT")

            with (path / (name + ".json")).open("w") as f:
                json.dump(info, f)
            if "timed_out" in info and info["timed_out"] is True:
                print(
                    "Breaking from loop due timeout (larger automata would require more time still)"
                )
                break
            if "cost" in info and info["cost"] == 0:
                print(
                    "Found glitchless solution, more states will not change that. Stopping..."
                )
                break


if __name__ == "__main__":
    main()
