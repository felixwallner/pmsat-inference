import sys
import threading
import time
from concurrent.futures._base import TimeoutError
from dataclasses import dataclass, field
from typing import Any, Collection, Iterable

from pebble import concurrent
from pysat.examples.fm import FM
from pysat.examples.lsu import LSU
from pysat.examples.rc2 import RC2, RC2Stratified
from pysat.formula import WCNF

__all__ = ["SATVariable", "PMSATProblem"]


@dataclass(frozen=True)
class SATVariable:
    pass


@dataclass(order=False)
class PMSATProblem:
    _var2index: dict[SATVariable, int] = field(init=False, repr=False, default_factory=dict)
    # No var with index 0 because we cannot distinguish between +0 and -0
    _index2var: list[None | SATVariable] = field(
        init=False, repr=False, default_factory=lambda: [None]
    )
    _clauses: set[tuple[frozenset[int], int]] = field(init=False, repr=False, default_factory=set)

    @property
    def num_variables(self) -> int:
        return len(self._index2var) - 1

    @property
    def num_clauses(self) -> int:
        return len(self._clauses)

    @property
    def num_hard(self) -> int:
        return sum(1 for _, weight in self._clauses if weight <= 0)

    @property
    def num_soft(self) -> int:
        return sum(1 for _, weight in self._clauses if weight > 0)

    def __repr__(self) -> str:
        num_variables = self.num_variables
        num_clauses = self.num_clauses
        num_hard = self.num_hard
        num_soft = self.num_soft
        return f"{self.__class__.__name__}({num_variables=}, {num_clauses=}, {num_hard=}, {num_soft=})"

    def _add_var_and_map_var_to_index(self, var: SATVariable) -> int:
        if var in self._var2index.keys():
            return self._var2index[var]
        n = len(self._index2var)
        self._index2var.append(var)
        self._var2index[var] = n
        return n

    def _add_clause(self, clause: Collection[int], weight: int = -1) -> None:
        if clause:
            self._clauses.add((frozenset(clause), weight))

    def to_WCNF(self) -> WCNF:
        wcnf = WCNF()
        for clause, weight in self._clauses:
            if weight > 0:
                wcnf.append(clause, weight)
            else:
                wcnf.append(clause)
        return wcnf

    def solution_from_state_assignment(self, state_assignment: Iterable[int]) -> list[SATVariable]:
        only_variables = filter(lambda l: abs(l) < len(self._index2var), state_assignment)
        only_true = filter(lambda x: x > 0, only_variables)
        solution: list[SATVariable] = list(map(lambda x: self._index2var[x], only_true))  # type: ignore
        return solution

    def add_clause(
        self, positive: Iterable[SATVariable], negative: Iterable[SATVariable], weight: int = -1
    ) -> None:
        """Add a SAT clause that sets positive variables true and negative variables false.
        If a non-negative weight is given the clause will be soft with the given weight.
        Otherwise, a negative weight will result in a hard clause."""
        clause = set()
        for var in positive:
            clause.add(self._add_var_and_map_var_to_index(var))
        for var in negative:
            clause.add(-self._add_var_and_map_var_to_index(var))
        self._add_clause(clause, weight)

    def add_exactly_one_true_clauses(self, vars: Iterable[SATVariable]) -> None:
        """Add clauses such that exactly one single var of vars may and must be true"""
        v = list(map(self._add_var_and_map_var_to_index, set(vars)))

        # first add: (v0 | v1 | ... | vn)
        self._add_clause(v)

        # then add: (!vi | !vj) to prevent any two variables being true at the same time
        for i in range(0, len(v) - 1):
            for j in range(i + 1, len(v)):
                self._add_clause([-v[i], -v[j]])

    def add_max_one_true_clauses(self, vars: Iterable[SATVariable]) -> None:
        """Add clauses such that at most one single var of vars may be true"""
        v = list(map(self._add_var_and_map_var_to_index, set(vars)))

        # add: (!vi | !vj) to prevent any two variables being true at the same time
        for i in range(0, len(v) - 1):
            for j in range(i + 1, len(v)):
                self._add_clause([-v[i], -v[j]])

    def _solve_lsu(
        self,
        info: dict[str, Any] | None = None,
        timeout: int | float | None = None,
        print_debug: bool = False,
    ) -> list[SATVariable] | None:
        """Solves the pmsat problem and returns a set of true variables that satisfies the problem
        or None if it is unsatisfiable.
        The function also returns after a given timeout or if interrupted with a KeyboardInterrupt.
        In that case the solution will be the best possible solution until that point in time.

        Will set the following variables in info if given a dict:
            num_vars, num_hard, num_soft: the number of SAT variables, hard and soft clauses
            is_sat: whether the problem was satisfiable. Equivalent to: solution is not None
            optimal_solution: If True then the SAT solving was interrupted via timeout or KeyboardInterrupt.
                              In that case, it cannot be guaranteed that the solution is MaxSAT
            solve_time, solve_perf_counter: time and perf_counter_time used for solving
            and cost (only if is_sat == True): Number of unfullfilled soft clauses
        """
        if info is None:
            info = dict()

        wcnf = self.to_WCNF()
        info["num_vars"] = wcnf.nv
        info["num_hard"] = len(wcnf.hard)
        info["num_soft"] = len(wcnf.soft)

        interrupted = False

        @concurrent.process
        def instrument_solver(wcnf, timeout):
            solver = LSU(wcnf, verbose=2 if print_debug else 0, expect_interrupt=True)

            @concurrent.thread
            def actual_solve(solver) -> bool:
                return solver.solve()  # returns solveable

            @concurrent.thread
            def kill_process() -> bool:
                time.sleep(5)
                sys.exit()

            timed_out = False
            interrupted = False
            future = actual_solve(solver)
            try:
                future.result(timeout=timeout)
            except TimeoutError:
                interrupted = True
                timed_out = True
            is_sat = hasattr(solver, "model") and solver.get_model() is not None
            optimum = not interrupted
            if is_sat:
                model, cost = list(solver.get_model()), solver.cost
                kill_process()
                return model, cost, optimum, timed_out
            kill_process()
            return None, None, optimum, timed_out

        model, cost, optimum, timed_out = None, None, False, False
        start_time = time.time()
        start_perf_counter = time.perf_counter()
        future = instrument_solver(wcnf, timeout)
        try:
            model, cost, optimum, timed_out = future.result()
        except KeyboardInterrupt:
            future.cancel()
        delta_perf_counter = time.perf_counter() - start_perf_counter
        delta_time = time.time() - start_time
        is_sat = model is not None
        info["timed_out"] = timed_out
        info["is_sat"] = is_sat
        info["optimal_solution"] = optimum
        info["solve_time"] = delta_time
        info["solve_perf_counter"] = delta_perf_counter
        if model is None:
            return None

        solution = self.solution_from_state_assignment(model)
        info["cost"] = cost
        return solution

    def _solve_fm(
        self,
        info: dict[str, Any] | None = None,
        timeout: int | float | None = None,
        print_debug: bool = False,
    ) -> list[SATVariable] | None:
        """Solves the pmsat problem and returns a set of true variables that satisfies the problem
        or None if it is unsatisfiable.
        The function also returns after a given timeout or if interrupted with a KeyboardInterrupt.
        In that case the solution will be the best possible solution until that point in time.

        Will set the following variables in info if given a dict:
            num_vars, num_hard, num_soft: the number of SAT variables, hard and soft clauses
            is_sat: whether the problem was satisfiable. Equivalent to: solution is not None
            optimal_solution: If True then the SAT solving was interrupted via timeout or KeyboardInterrupt.
                              In that case, it cannot be guaranteed that the solution is MaxSAT
            solve_time, solve_perf_counter: time and perf_counter_time used for solving
            and cost (only if is_sat == True): Number of unfullfilled soft clauses
        """
        if info is None:
            info = dict()

        wcnf = self.to_WCNF()
        info["num_vars"] = wcnf.nv
        info["num_hard"] = len(wcnf.hard)
        info["num_soft"] = len(wcnf.soft)

        interrupted = False
        model = None
        cost = None

        solver = FM(wcnf, verbose=2 if print_debug else 0)

        @concurrent.process(timeout=timeout)
        def actual_solve(prep_solver):
            solvable = prep_solver.compute()
            if solvable:
                return prep_solver.model, prep_solver.cost
            return None, None

        start_time = time.time()
        start_perf_counter = time.perf_counter()
        future = actual_solve(solver)
        try:
            model, cost = future.result()
        except TimeoutError:
            info["timed_out"] = True
            interrupted = True
        except KeyboardInterrupt:
            future.cancel()
            interrupted = True

        delta_perf_counter = time.perf_counter() - start_perf_counter
        delta_time = time.time() - start_time
        is_sat = model is not None
        info["is_sat"] = is_sat
        info["optimal_solution"] = not interrupted
        info["solve_time"] = delta_time
        info["solve_perf_counter"] = delta_perf_counter
        if model is None:
            return None

        solution = self.solution_from_state_assignment(model)
        info["cost"] = cost
        return solution

    def _solve_rc2(
        self,
        info: dict[str, Any] | None = None,
        timeout: int | float | None = None,
        print_debug: bool = False,
    ) -> list[SATVariable] | None:
        """Solves the pmsat problem and returns a set of true variables that satisfies the problem
        or None if it is unsatisfiable.
        The function also returns after a given timeout or if interrupted with a KeyboardInterrupt.
        In that case the solution will be the best possible solution until that point in time.

        Will set the following variables in info if given a dict:
            num_vars, num_hard, num_soft: the number of SAT variables, hard and soft clauses
            is_sat: whether the problem was satisfiable. Equivalent to: solution is not None
            optimal_solution: If True then the SAT solving was interrupted via timeout or KeyboardInterrupt.
                              In that case, it cannot be guaranteed that the solution is MaxSAT
            solve_time, solve_perf_counter: time and perf_counter_time used for solving
            and cost (only if is_sat == True): Number of unfullfilled soft clauses
        """
        if info is None:
            info = dict()

        wcnf = self.to_WCNF()
        info["num_vars"] = wcnf.nv
        info["num_hard"] = len(wcnf.hard)
        info["num_soft"] = len(wcnf.soft)

        interrupted = False
        model = None
        cost = None

        solver = RC2(wcnf, verbose=2 if print_debug else 0)

        @concurrent.process(timeout=timeout)
        def actual_solve(prep_solver):
            solved_model = prep_solver.compute()
            return solved_model, prep_solver.cost

        start_time = time.time()
        start_perf_counter = time.perf_counter()
        future = actual_solve(solver)
        try:
            model, cost = future.result()
        except TimeoutError:
            info["timed_out"] = True
            interrupted = True
        except KeyboardInterrupt:
            future.cancel()
            interrupted = True

        delta_perf_counter = time.perf_counter() - start_perf_counter
        delta_time = time.time() - start_time
        is_sat = model is not None
        info["is_sat"] = is_sat
        info["optimal_solution"] = not interrupted
        info["solve_time"] = delta_time
        info["solve_perf_counter"] = delta_perf_counter
        if model is None:
            return None

        solution = self.solution_from_state_assignment(model)
        info["cost"] = cost
        return solution
