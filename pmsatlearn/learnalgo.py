import itertools as it
import sys
from collections import defaultdict
from dataclasses import dataclass
from functools import partial
from numbers import Number
from typing import Any, Literal, TypeAlias

from aalpy.automata import MealyMachine, MealyState, MooreMachine, MooreState

from .pmsat import PMSATProblem, SATVariable

__all__ = ["run_pmSATLearn"]

Input: TypeAlias = int | str
Output: TypeAlias = int | str


@dataclass(frozen=True)
class Delta(SATVariable):
    """Transition function from state s0 with input i to state s1"""

    s0: int
    i: Input
    s1: int

    def latex(self) -> str:
        return f"\delta(s_{self.s0}, \\text{{{self.i}}}, s_{self.s1})"


@dataclass(frozen=True)
class LambdaState(SATVariable):
    """Output o of state s - state output of a Moore machine"""

    s: int
    o: Output

    def latex(self) -> str:
        return f"\lambda(s_{self.s}, \\text{{{self.o}}})"


@dataclass(frozen=True)
class LambdaTransition(SATVariable):
    """Output o of transition i from s - transition output of a Mealy machine"""

    s: int
    i: Input
    o: Output

    def latex(self) -> str:
        raise NotImplementedError


@dataclass(frozen=True, order=True)
class StepState(SATVariable):
    """Correspondance of a step of a trace to a state s"""

    trace: int
    step: int
    s: int

    def latex(self) -> str:
        return f"\omega(t_{self.trace}, {self.step}, s_{self.s})"


@dataclass(frozen=True, order=True)
class StepGlitch(SATVariable):
    """Marks the step of trace  as a glitch - which then does not enforce transition"""

    trace: int
    step: int

    def latex(self) -> str:
        return f"\mathcal{{G}}(t_{self.trace}, {self.step})"


def _build_moore_problem(
    n_states: int,
    inputs: list,
    outputs: list,
    traces: list[list],
    cost_per_step: int = 1,
    cost_per_transition: int = 0,
    allow_different_initial_states: bool = False,
) -> PMSATProblem:
    if cost_per_step > 0 and cost_per_transition > 0:
        print("WARN: cost_per_step AND cost_per_transition were set. Was this on purpose?")
    cost_per_step = max(cost_per_step, 0)
    cost_per_transition = max(cost_per_transition, 0)
    if not cost_per_step and not cost_per_transition:
        raise ValueError("Neither cost_per_step nor cost_per_transition was set")
    if cost_per_transition:
        raise NotImplementedError("cost_per_transition is not implemented yet")
    if allow_different_initial_states:
        raise NotImplementedError("allow_different_initial_states is not implemented yet")

    problem = PMSATProblem()
    # traces == [
    #     [o0, (i1, o1), (i2, o2), ..., (im, om)], # trace 0 of len m + 1
    #     [o0, (i1, o1)], # trace 1
    #     [o0, (i1, o1), (i2, o2)], # trace 2
    # ]

    for s0 in range(n_states):
        # for each state there can only be one output (= Moore states)
        problem.add_exactly_one_true_clauses(map(partial(LambdaState, s0), outputs))
        # but, there must be at least one state with a given output
        # fix the first len(outputs) states output
        if s0 < len(outputs):
            problem.add_clause([LambdaState(s0, outputs[s0])], [])

        for i in inputs:
            # for each s0 and i there can be only one s1 (= deterministic transitions)
            problem.add_exactly_one_true_clauses(map(partial(Delta, s0, i), range(n_states)))

    for trace_index, trace in enumerate(traces):
        n_steps = len(trace)
        # trace == [o0, (i1, o1), (i2, o2), ...]

        if not allow_different_initial_states:
            # The first step of every trace (k=0) always corresponds to the inital states output
            # this may lead to UNSAT if any traces differ in their initial output
            # in other words, adding this clause expects correct resets
            problem.add_clause([StepState(trace_index, 0, outputs.index(trace[0]))], [])
        else:
            assert False, "TODO: Impliment allowing different starting states for different traces"

        for k in range(1, n_steps):
            # each step in trace has only one post-state (= state_k with output_k after applying input_k)
            problem.add_exactly_one_true_clauses(
                map(partial(StepState, trace_index, k), range(n_states))
            )

            if cost_per_step:
                # cost is calculated based on each step of a trace marked as glitch
                # try setting all glitches false but set as soft constraint
                problem.add_clause([], [StepGlitch(trace_index, k)], weight=cost_per_step)

            input_k, output_k = trace[k][:2]
            for s in range(n_states):
                # output_k in trace corresponds to post-state s:
                # (state_k = s) => (Lambda(s) = output_k) <==>
                # !(state_k = s) | (Lambda(s) = output_k)
                problem.add_clause([LambdaState(s, output_k)], [StepState(trace_index, k, s)])

                for s1 in range(n_states):
                    # each step for which no valid transition exists is a glitch
                    # (state_k-1 = s) & (state_k = s1) & !(Delta(s, input_k) = s1) => glitch_k <==>
                    # !(state_k-1 = s) | !(state_k = s1) | (Delta(s, input_k) = s1) | glitch_k
                    # TODO: for cost_per_transition -> (Sk-1 = m) & (Sk = n) & Gk => (DeltaG(m, Ik) = n)
                    problem.add_clause(
                        [Delta(s, input_k, s1), StepGlitch(trace_index, k)],
                        [
                            StepState(trace_index, k - 1, s),
                            StepState(trace_index, k, s1),
                        ],
                    )

    return problem


def build_det_mm(
    solution: list[SATVariable], traces: list[list], info: dict | None = None
) -> MooreMachine:
    vglitches: list[StepGlitch] = sorted(filter(lambda x: isinstance(x, StepGlitch), solution))
    vlamda: list[LambdaState] = sorted(
        filter(lambda x: isinstance(x, LambdaState), solution), key=lambda l: l.s
    )
    vdelta: list[Delta] = list(filter(lambda x: isinstance(x, Delta), solution))
    vstep: list[StepState] = sorted(filter(lambda x: isinstance(x, StepState), solution))

    mm_states = [MooreState(l.s, l.o) for l in vlamda]
    for trace_index, trace in enumerate(traces):
        tracesteps = list(step for step in vstep if step.trace == trace_index)
        traceglitches = set(glitch.step for glitch in vglitches if glitch.trace == trace_index)
        last_step: StepState | None = None
        for step_index, (data_step, var_step) in enumerate(zip(trace, tracesteps)):
            assert step_index == var_step.step
            if last_step is None:
                # beginning of trace
                last_step = var_step
                continue
            if step_index not in traceglitches:
                step_delta = Delta(last_step.s, data_step[0], var_step.s)
                assert step_delta in vdelta
                assert (
                    step_delta.i not in mm_states[step_delta.s0].transitions.keys()
                    or mm_states[step_delta.s0].transitions[step_delta.i]
                    == mm_states[step_delta.s1]
                )
                mm_states[step_delta.s0].transitions[step_delta.i] = mm_states[step_delta.s1]
            last_step = var_step

    assert vstep[0].trace == vstep[0].step == 0, "Vsteps are not sorted, expected initial state"
    mm = MooreMachine(mm_states[vstep[0].s], mm_states)

    if info is not None:
        # number of dominant-reachable states
        info["dominant_reachable_states"] = 1 + sum(
            bool(mm.get_shortest_path(mm.initial_state, state))
            for state in mm.states
            if state is not mm.initial_state
        )

    return mm


def build_stochastic_vis_mm(
    solution: list[SATVariable], traces: list[list], info: dict | None = None
) -> MooreMachine:
    vglitches: list[StepGlitch] = sorted(filter(lambda x: isinstance(x, StepGlitch), solution))
    vlamda: list[LambdaState] = sorted(
        filter(lambda x: isinstance(x, LambdaState), solution), key=lambda l: l.s
    )
    vdelta: set[Delta] = set(filter(lambda x: isinstance(x, Delta), solution))
    vstep: list[StepState] = sorted(filter(lambda x: isinstance(x, StepState), solution))

    mm_states = [MooreState(l.s, l.o) for l in vlamda]
    assert mm_states[0].state_id == 0, "Moore state are not sorted"

    glitches: set[Delta] = set()
    counter = defaultdict(int)

    for trace_index, trace in enumerate(traces):
        tracesteps = list(step for step in vstep if step.trace == trace_index)
        traceglitches = set(glitch.step for glitch in vglitches if glitch.trace == trace_index)
        last_step: StepState | None = None
        for step_index, (data_step, var_step) in enumerate(zip(trace, tracesteps)):
            assert step_index == var_step.step
            if last_step is None:
                # beginning of trace
                last_step = var_step
                continue
            step_delta = Delta(last_step.s, data_step[0], var_step.s)
            counter[step_delta] += 1
            if step_index in traceglitches:
                glitches.add(step_delta)
                if step_delta in vdelta:
                    print(
                        f"NOT OPTIMAL: {step_delta} in vdelta but step t={trace_index}, step={step_index} was marked as glitch!",
                        file=sys.stderr,
                    )

                # assert step_delta not in vdelta
            else:
                assert (
                    step_delta in vdelta
                ), f"ERR: {step_delta} NOT in {vdelta}! Trace steps: {tracesteps}, Trace glitches: {traceglitches}, counter: {counter}"
            last_step = var_step

    for delta, occur in counter.items():
        s1, s2 = mm_states[delta.s0], mm_states[delta.s1]
        if delta in vdelta:
            stochastic_input = f"{delta.i} ({occur})"
        else:
            stochastic_input = f"!{delta.i} [{occur}]"
            for i in range(1, 1000):
                if stochastic_input not in s1.transitions:
                    break
                stochastic_input = f"!{delta.i} [{occur}] t{i}"
        s1.transitions[stochastic_input] = s2

    assert vstep[0].trace == vstep[0].step == 0, "Vsteps are not sorted, expected initial state"
    mm = MooreMachine(mm_states[vstep[0].s], mm_states)

    if info is not None:
        # frequencies of glitch deltas
        info["glitched_delta_freq"] = [counter[delta] for delta in glitches]
        # degree of input completeness
        # frequencies of non-glitch deltas
        info["dominant_delta_freq"] = [counter.get(delta, 0) for delta in vdelta]
        assert (
            len(info["dominant_delta_freq"]) == len(info["input_alphabet"]) * info["num_states"]
        ), f'{info["dominant_delta_freq"]} len({len(info["dominant_delta_freq"])}) != {info["input_alphabet"]} len({len(info["input_alphabet"])}) * {info["num_states"]}'

    return mm


def run_pmSATLearn(
    data: list[list[Output | tuple[Input, Output]]],
    n_states: int,
    automata_type: Literal["mealy", "moore"],
    pm_strategy: Literal["lsu", "fm", "rc2"] = "lsu",
    timeout: int | float | None = None,
    cost_scheme: Literal["per_step", "per_transition"] = "per_step",
    print_info: bool = True,
) -> (
    tuple[MealyMachine, MealyMachine, dict[str, Any]]
    | tuple[MooreMachine, MooreMachine, dict[str, Any]]
    | tuple[None, None, dict[str, Any]]
):
    allowed_automata_types = ["mealy", "moore"]
    if automata_type not in allowed_automata_types:
        raise TypeError(
            f"automata type was {automata_type} but should be in {allowed_automata_types}"
        )
    if automata_type == "mealy":
        raise NotImplementedError("Mealy machines are not supported yet")

    allowed_cost_schemes = ["per_step", "per_transition"]
    if cost_scheme not in allowed_cost_schemes:
        raise TypeError(f"cost_scheme was {cost_scheme} but should be in {allowed_cost_schemes}")
    if cost_scheme == "per_transition":
        raise NotImplementedError("Cost scheme 'per_transition' is not supported yet")

    if not isinstance(n_states, int) or n_states < 1:
        raise TypeError("Number of states (n) should be a positive non-zero integer")

    if timeout is not None and (not isinstance(timeout, Number) or timeout < 0):
        raise TypeError("Expected timeout to be None or a positive number")

    # automata_type == "moore" =>
    # data == [
    #     [o0, (i1, o1), (i2, o2), ..., (il, ol)], # trace len l+1
    #     [o0, (i1, o1)], # trace 2
    # ]
    inputs = sorted(set((step[0] for trace in data for step in trace[1:])))
    outputs = sorted(
        set(
            it.chain(
                (trace[0] for trace in data), (step[1] for trace in data for step in trace[1:])
            )
        )
    )
    if print_info:
        print("INFO: Inputs:", inputs)
        print("INFO: Outputs:", outputs)

    info = {
        "is_sat": False,
        "num_states": n_states,
        "automata_type": automata_type,
        "solve_time": 0.0,
        "solve_perf_counter": 0.0,
        "input_alphabet": inputs,
        "output_alphabet": outputs,
        "optimal_solution": True,
        "timed_out": False,
    }

    if n_states < len(outputs):
        print(
            f"WARN: UNSAT: The automata cannot be learned with only {n_states} states while having {len(outputs)} outputs"
        )
        return None, None, info

    problem = _build_moore_problem(n_states, inputs, outputs, data)
    ### comment in to write problem as Latex Code
    # with open("problem.log", "w") as f:
    #     formula = []
    #     for clause, weight in problem._clauses:
    #         write_clause = []
    #         for index in sorted(clause):
    #             var = problem._index2var[abs(index)]
    #             if index > 0:
    #                 write_clause.append(var.latex())
    #             else:
    #                 write_clause.append(f"\overline{{{var.latex()}}}")
    #         formula.append("(" + " \lor ".join(write_clause) + ")")
    #     f.write(" \land \\\\".join(formula))

    if pm_strategy == "lsu":
        solution = problem._solve_lsu(info, timeout, print_info)
    elif pm_strategy == "fm":
        solution = problem._solve_fm(info, timeout, print_info)
    elif pm_strategy == "rc2":
        solution = problem._solve_rc2(info, timeout, print_info)
    else:
        raise TypeError(f"pm_strategy should be 'lsu', 'fm' or 'rc2', but is {pm_strategy}")
    did_timeout = info["timed_out"]
    if solution is None:
        print("WARN: UNSAT: ", end="")
        if did_timeout:
            print("The learning timed out before finding a satisfiable solution")
        else:
            print("The learning was interrupted before finding a satisfiable solution")
        return None, None, info
    elif did_timeout:
        print("WARN: Learning timed out - potentially sub-optimal solution")
    elif not info["optimal_solution"]:
        print("WARN: Learning was interrupted - potentially sub-optimal solution")

    ### comment in to write solution as Latex Code
    # with open("solution.log", "w") as f:
    #     f.write(" \land ".join(map(lambda var: var.latex(), solution)))

    vglitches: list[StepGlitch] = sorted(filter(lambda x: isinstance(x, StepGlitch), solution))
    glitch_steps = [(glitch.trace, glitch.step) for glitch in vglitches]
    info["glitch_steps"] = glitch_steps
    if print_info:
        print("INFO: Glitches:", glitch_steps)
        print("INFO: # Glitches:", len(glitch_steps))

    det_mm = build_det_mm(solution, data, info)
    stochastic_vis_mm = build_stochastic_vis_mm(solution, data, info)
    return det_mm, stochastic_vis_mm, info
