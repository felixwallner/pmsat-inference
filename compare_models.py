import argparse
import itertools as it
import queue
import sys

from aalpy.automata import MealyState, MooreMachine, MooreState
from aalpy.base import AutomatonState, DeterministicAutomaton
from aalpy.utils import load_automaton_from_file


def bisimilar(a1: DeterministicAutomaton, a2: DeterministicAutomaton):
    """
    Checks whether the provided moore machines are bisimilar
    """

    to_check = queue.Queue[tuple[AutomatonState, AutomatonState]]()
    to_check.put((a1.initial_state, a2.initial_state))
    requirements = dict()
    requirements[(a1.initial_state, a2.initial_state)] = []

    while not to_check.empty():
        s1, s2 = to_check.get()

        if (isinstance(s1, MooreState) and s1.output != s2.output) or (
            isinstance(s1, MealyState) and s1.output_fun != s2.output_fun
        ):
            return requirements[(s1, s2)]

        t1, t2 = s1.transitions, s2.transitions
        for t in it.chain(t1.keys(), t2.keys()):
            if (t in t1.keys()) != (t in t2.keys()):
                return requirements[(s1, s2)] + [t]

        for t in t1.keys():
            c1, c2 = t1[t], t2[t]
            if (c1, c2) not in requirements:
                requirements[(c1, c2)] = requirements[(s1, s2)] + [t]
                to_check.put((c1, c2))


def parse_args(cmd_args):
    parser = argparse.ArgumentParser()
    parser.add_argument("moore_dot_file1", type=str, help="Automata to compare")
    parser.add_argument("moore_dot_file2", type=str, help="Automata to compare")
    args = parser.parse_args(cmd_args)
    return args


def main(cmd_args: list[str] | None = None):
    args = parse_args(sys.argv[1:] if cmd_args is None else cmd_args)
    file1 = args.moore_dot_file1
    file2 = args.moore_dot_file2

    mm1: MooreMachine = load_automaton_from_file(file1, "moore", True)
    mm2: MooreMachine = load_automaton_from_file(file2, "moore", True)
    inputs1 = sorted(set(mm1.get_input_alphabet()))
    inputs2 = sorted(set(mm2.get_input_alphabet()))
    outputs1 = sorted(set(state.output for state in mm1.states))
    outputs2 = sorted(set(state.output for state in mm2.states))
    assert mm1.is_minimal(), "Machine 1 was not minimal!"
    assert mm2.is_minimal(), "Machine 2 was not minimal!"

    cex = bisimilar(mm1, mm2)
    if cex is None:
        print("Automata are bisimilar!")
        return 0
    print("CEX for bisimilarity:", cex)
    print(
        "Automata 1:", [mm1.initial_state.output] + mm1.compute_output_seq(mm1.initial_state, cex)
    )
    print(
        "Automata 2:", [mm2.initial_state.output] + mm2.compute_output_seq(mm2.initial_state, cex)
    )

    if inputs1 != inputs2:
        print("Automata have different input alphabets!")
        print(1, inputs1)
        print(2, inputs2)
        return 1

    if outputs1 != outputs2:
        print("Automata have different output alphabets!")
        print(1, outputs1)
        print(2, outputs2)
        return 1

    return 1


if __name__ == "__main__":
    main()
