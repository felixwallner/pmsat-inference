import argparse
import itertools as it
import queue
import random
import statistics
import sys
from typing import Any

from aalpy.automata import MealyState, MooreMachine, MooreState
from aalpy.base import Automaton, AutomatonState, DeterministicAutomaton
from aalpy.oracles import RandomWalkEqOracle
from aalpy.utils import load_automaton_from_file


def bisimilar(a1: DeterministicAutomaton, a2: DeterministicAutomaton):
    """
    Checks whether the provided moore machines are bisimilar
    """

    to_check = queue.Queue[tuple[AutomatonState, AutomatonState]]()
    to_check.put((a1.initial_state, a2.initial_state))
    requirements = dict()
    requirements[(a1.initial_state, a2.initial_state)] = []

    while not to_check.empty():
        s1, s2 = to_check.get()

        if (isinstance(s1, MooreState) and s1.output != s2.output) or (
            isinstance(s1, MealyState) and s1.output_fun != s2.output_fun
        ):
            return requirements[(s1, s2)]

        t1, t2 = s1.transitions, s2.transitions
        for t in it.chain(t1.keys(), t2.keys()):
            if (t in t1.keys()) != (t in t2.keys()):
                return requirements[(s1, s2)] + [t]

        for t in t1.keys():
            c1, c2 = t1[t], t2[t]
            if (c1, c2) not in requirements:
                requirements[(c1, c2)] = requirements[(s1, s2)] + [t]
                to_check.put((c1, c2))


def stochastic_conformance(
    automaton_generator: Automaton,
    automaton_tester: Automaton,
    num_samples: int = 10000,
    reset_prob: float | None = None,
) -> float:
    """Calculate the one-way stochastic conformance between two automata by sampling
    from automaton_generator using random walks and replaying the samples on automaton_tester.

    automaton_generator: Automaton - automaton from which to sample
    automaton_tester: Automaton - automaton on which to replay samples
    num_samples: int - number of samples (walks) to samples from automaton_generator
    reset_prob: float | None - if float use fixed reset probability else use 1/automaton_generator.size

    return: float - proportion of correctly replayed samples (0..1)

    Notes:
    * Generates only from enabled inputs and will reset if no enabled inputs exist in a state
    """
    if reset_prob is not None and reset_prob <= 0:
        raise ValueError("Reset Probability should be > 0 or None")

    inputs_generator = sorted(set(automaton_generator.get_input_alphabet()))
    inputs_tester = sorted(set(automaton_tester.get_input_alphabet()))

    if inputs_generator != inputs_tester:
        print("Automata have different input alphabets!")
        print("Generator", inputs_generator)
        print("Tester", inputs_tester)
        raise ValueError("Only automaton with the same input alphabet can be compared")

    if isinstance(automaton_generator, DeterministicAutomaton) and isinstance(
        automaton_tester, DeterministicAutomaton
    ):
        cex = bisimilar(automaton_generator, automaton_tester)
        if cex is None:
            # print("Automata are bisimilar!")
            return 1
        # print("Automata are NOT bisimilar, CEX:", cex)

    samples = []
    reset_prob = reset_prob or 1 / automaton_generator.size
    # inputs = automaton_generator.get_input_alphabet()
    for _ in range(num_samples):
        samples.append([])
        automaton_generator.reset_to_initial()
        while True:
            if not automaton_generator.current_state.transitions.keys():
                break
            inp = random.choice(list(automaton_generator.current_state.transitions.keys()))
            out = automaton_generator.step(inp)
            samples[-1].append((inp, out))
            if random.random() < reset_prob:
                break
    assert len(samples) == num_samples

    def replay(automaton: Automaton, sample: list[tuple[Any, Any]]) -> bool:
        automaton.reset_to_initial()
        if isinstance(automaton, DeterministicAutomaton):
            for inp, out in sample:
                if not inp in automaton.current_state.transitions.keys():
                    return False
                if automaton.step(inp) != out:
                    return False
        else:
            assert hasattr(automaton, "step_to")
            for inp, out in sample:
                if not inp in automaton.current_state.transitions.keys():
                    return False
                if automaton.step_to(inp, out) is None:
                    return False
        return True

    correct = sum(replay(automaton_tester, sample) for sample in samples)
    return correct / num_samples


def f_score(
    automaton1: Automaton,
    automaton2: Automaton,
    num_samples: int = 10000,
    reset_prob: float | None = None,
) -> float:
    # precision and recall assuming automaton1 is ground truth and automaton2 is other
    precision = stochastic_conformance(
        automaton2, automaton1, num_samples=num_samples, reset_prob=reset_prob
    )
    recall = stochastic_conformance(
        automaton1, automaton2, num_samples=num_samples, reset_prob=reset_prob
    )
    f_score = statistics.harmonic_mean((precision, recall))
    # print(f"{precision=}, {recall=}, {f_score=}")
    return f_score


def parse_args(cmd_args):
    parser = argparse.ArgumentParser()
    parser.add_argument("dot_file_gt", type=str, help="Ground truth automata")
    parser.add_argument("dot_file_other", type=str, help="Automata to compare to")
    parser.add_argument(
        "-file_gt_type", "-tg", type=str, default="moore", help="Type of ground truth automaton"
    )
    parser.add_argument(
        "-file_other_type", "-to", type=str, default="moore", help="Type of other automaton"
    )
    args = parser.parse_args(cmd_args)
    return args


def main(cmd_args: list[str] | None = None):
    args = parse_args(sys.argv[1:] if cmd_args is None else cmd_args)
    file_gt = args.dot_file_gt
    file_other = args.dot_file_other
    file_gt_type = args.file_gt_type
    file_other_type = args.file_other_type
    samples = 10_000

    ground_truth = load_automaton_from_file(file_gt, file_gt_type)
    other = load_automaton_from_file(file_other, file_other_type)
    precision = stochastic_conformance(other, ground_truth, num_samples=samples)
    recall = stochastic_conformance(ground_truth, other, num_samples=samples)
    f_score = statistics.harmonic_mean((precision, recall))
    print(f"{precision=}, {recall=}, {f_score=}")


if __name__ == "__main__":
    main()
