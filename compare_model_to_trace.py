import argparse
import json
import sys
from pprint import pp

from aalpy.automata import MooreMachine
from aalpy.utils import load_automaton_from_file


def parse_args(cmd_args):
    parser = argparse.ArgumentParser()
    parser.add_argument("moore_dot_file", type=str, help="Automata to compare")
    parser.add_argument("trace_file", type=str, help="Trace to replay on automaton")
    args = parser.parse_args(cmd_args)
    return args


def main(cmd_args: list[str] | None = None):
    args = parse_args(sys.argv[1:] if cmd_args is None else cmd_args)
    file = args.moore_dot_file
    file_traces = args.trace_file

    mm: MooreMachine = load_automaton_from_file(file, "moore", True)
    with open(file_traces, "r") as f:
        traces = json.load(f)
    if type(traces) == dict:
        traces = traces["traces"]
    assert type(traces) == list and all(type(trace) == list for trace in traces)

    inputs = sorted(set(mm.get_input_alphabet()))
    outputs = sorted(set(state.output for state in mm.states))
    print("inputs", inputs)
    pp(["outputs", outputs, len(outputs)])
    assert mm.is_minimal(), "Machine was not minimal!"

    inputs_traces = sorted(set(i for trace in traces for i, _ in trace[1:]))
    outputs_traces = sorted(
        set(trace[0] for trace in traces) | set(o for trace in traces for _, o in trace[1:])
    )
    print("inputs_traces", inputs_traces)
    pp(["outputs_traces", outputs_traces, len(outputs_traces)])

    if inputs != inputs_traces:
        print("Inputs differ!")
        exit()
    if outputs != outputs_traces:
        print("Outputs differ!")
        exit()

    wrong = set()
    state = mm.initial_state
    for index, trace in enumerate(traces):
        input_seq = [i for i, _ in trace[1:]]
        output_seq = [trace[0]] + [o for _, o in trace[1:]]
        out = [state.output] + mm.execute_sequence(state, input_seq)
        if out != output_seq:
            wrong.add(index)
        if index == 709:
            print(f"Differ for input seq", input_seq)
            print("Machine", out)
            print("Trace", output_seq)
            mm.reset_to_initial()
            print("State", mm.current_state.state_id)
            for i in input_seq:
                mm.step(i)
                print("State", mm.current_state.state_id)

    print(f"{len(traces)} traces with length {sum(len(t)-1 for t in traces)}")
    print(
        f"Total of {len(traces)} trace sequences of which {len(wrong)} cannot be replayed on machine ({len(wrong)/len(traces)*100})%!"
    )
    print("Wrong traces", sorted(wrong))


if __name__ == "__main__":
    main()
