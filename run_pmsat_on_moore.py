"""Take a given Moore machine in .dot format, 
generate traces from given method (with certain parameters),
potentially inject faults on purpose and finally
learn from the faulty traces using pmsat learn."""

import argparse
import copy
import hashlib
import itertools as it
import json
import queue
import random
import sys
import time
from pathlib import Path

from aalpy.automata import MealyState, MooreMachine, MooreState
from aalpy.base import SUL, AutomatonState, DeterministicAutomaton, Oracle
from aalpy.learning_algs import run_Alergia, run_KV, run_Lstar
from aalpy.oracles import (KWayTransitionCoverageEqOracle, RandomWalkEqOracle,
                           StatePrefixEqOracle)
from aalpy.SULs import MooreSUL
from aalpy.utils import load_automaton_from_file

from pmsatlearn import run_pmSATLearn


class PerfectEqOracle(Oracle):
    def __init__(self, alphabet: list, sul: SUL, mm: MooreMachine):
        super().__init__(alphabet, sul)
        self.mm = mm

    def find_cex(self, hypothesis):
        """
        Return a counterexample (inputs) that displays different behavior on system under learning and
        current hypothesis.

        Args:

          hypothesis: current hypothesis

        Returns:

            tuple or list containing counterexample inputs, None if no counterexample is found
        """
        self.reset_hyp_and_sul(hypothesis)
        # print(hypothesis)
        mm = self.mm  # mm: MooreMachine = copy.deepcopy(self.mm)
        assert set(mm.get_input_alphabet()) == set(
            self.alphabet
        ), f"{mm.get_input_alphabet()} != {self.alphabet}"
        dis = mm.find_distinguishing_seq(mm.current_state, hypothesis.current_state)
        if dis is None:
            return None  # no CEX found
        # print("Found cex:", dis)
        # return dis
        assert self.sul.step(None) == hypothesis.step(None)
        for index, inp in enumerate(dis):
            out1 = self.sul.step(inp)
            out2 = hypothesis.step(inp)
            if out1 != out2:
                assert (
                    index == len(dis) - 1
                ), f"Difference in output not on last index? {index} != {len(dis)-1}"
                return dis
                # print("Found difference in output without exception??")
        assert False, "Did not find difference in output on performing CEX??"


class TracedMooreSUL(MooreSUL):
    def __init__(self, mm: MooreMachine) -> None:
        super().__init__(mm)
        self.current_trace = [self.mm.step(None)]
        self.traces = [self.current_trace]

    def pre(self):
        super().pre()
        assert self.mm.current_state == self.mm.initial_state
        if len(self.current_trace) > 1:
            new_trace = [self.mm.step(None)]
            self.traces.append(new_trace)
            self.current_trace = new_trace

    def step(self, input, description: str | None = None):
        output = super().step(input)
        if input is None:
            return output
        if description is not None:
            entry = (input, output, description)
        else:
            entry = (input, output)
        self.traces[-1].append(entry)
        return output


class SingleTraceMooreSUL(TracedMooreSUL):
    def pre(self) -> None:
        """Reset with steps (only perfect knowledge allows for shortest path)"""
        path = self.mm.get_shortest_path(self.mm.current_state, self.mm.initial_state)
        for letter in path:
            self.step(letter)
        assert self.mm.current_state == self.mm.initial_state


def bisimilar(a1: DeterministicAutomaton, a2: DeterministicAutomaton):
    """
    Checks whether the provided moore machines are bisimilar
    """

    to_check = queue.Queue[tuple[AutomatonState, AutomatonState]]()
    to_check.put((a1.initial_state, a2.initial_state))
    requirements = dict()
    requirements[(a1.initial_state, a2.initial_state)] = []

    while not to_check.empty():
        s1, s2 = to_check.get()

        if (isinstance(s1, MooreState) and s1.output != s2.output) or (
            isinstance(s1, MealyState) and s1.output_fun != s2.output_fun
        ):
            return requirements[(s1, s2)]

        t1, t2 = s1.transitions, s2.transitions
        for t in it.chain(t1.keys(), t2.keys()):
            if (t in t1.keys()) != (t in t2.keys()):
                return requirements[(s1, s2)] + [t]

        for t in t1.keys():
            c1, c2 = t1[t], t2[t]
            if (c1, c2) not in requirements:
                requirements[(c1, c2)] = requirements[(s1, s2)] + [t]
                to_check.put((c1, c2))


def parse_args(cmd_args):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "moore_dot_file", type=str, help="Automata to learn and generated trace from"
    )
    parser.add_argument(
        "-generation_method", default="lstar", choices=["prefix", "rw", "lstar", "kv"]
    )
    parser.add_argument("-pmstrategy", default="rc2", choices=["lsu", "fm", "rc2"])
    parser.add_argument("-steps", type=int, default=1000, help="Number of steps for prefix and rw")
    parser.add_argument("-walk_len", type=int, default=10, help="Length of prefix walks")
    parser.add_argument("-reset_prob", type=float, default=0.09, help="Reset prob. for rw")
    parser.add_argument("-seed", type=int, default=None, help="Use fixed random seed")
    parser.add_argument("-nmax", type=int, default=None, help="Use up to nmax number of states")
    parser.add_argument(
        "-glitch_percent", type=float, default=0.01, help="Inject percent glitches"
    )
    parser.add_argument(
        "--use_single_trace",
        "--single",
        action="store_true",
        help="Generate a single trace instead of multple ones",
    )
    parser.add_argument(
        "--only_generate",
        action="store_true",
        help="Stop after generating the traces, i.e., do not learn",
    )

    args = parser.parse_args(cmd_args)
    if args.seed is None:
        args.seed = random.choice(range(2**32 - 1))
    assert (
        0 <= args.glitch_percent <= 1
    ), f"Glitch percent is {args.glitch_percent}, should be between 0 and 1"
    assert (
        args.generation_method not in ["prefix", "rw"] or args.steps is not None
    ), "Require -steps for methods prefix and rw"
    return args


def steps_traces(traces: list[list]) -> int:
    return sum(len(trace) - 1 for trace in traces)


def main(cmd_args: list[str] | None = None):
    args = parse_args(sys.argv[1:] if cmd_args is None else cmd_args)
    file = args.moore_dot_file
    mm: MooreMachine = load_automaton_from_file(file, "moore", True)
    inputs = sorted(set(mm.get_input_alphabet()))
    outputs = sorted(set(state.output for state in mm.states))
    assert mm.is_minimal(), "Machine was not minimal!"

    generation_method = args.generation_method
    n_states = len(mm.states)
    use_single_trace = args.use_single_trace
    steps = args.steps
    glitch_percent = args.glitch_percent
    seed = args.seed
    walk_len = args.walk_len
    reset_prob = args.reset_prob
    pmstrat = args.pmstrategy

    random.seed(seed)

    if use_single_trace:
        sul = SingleTraceMooreSUL(mm)
        assert mm.is_strongly_connected(), "Automaton must be strongly connected for single trace"
    else:
        sul = TracedMooreSUL(mm)

    auto_learned: MooreMachine | None = None
    if generation_method == "lstar":
        auto_learned = run_Lstar(
            inputs,
            sul,
            PerfectEqOracle(inputs, sul, mm),
            "moore",
            return_data=False,
            cache_and_non_det_check=False,
        )
        steps = max(steps - steps_traces(sul.traces), 0)
        generation_method = "prefix" if steps else "SKIP"
    elif generation_method == "kv":
        auto_learned = run_KV(
            inputs,
            sul,
            PerfectEqOracle(inputs, sul, mm),
            "moore",
            return_data=False,
            cache_and_non_det_check=False,
        )
        steps = max(steps - steps_traces(sul.traces), 0)
        generation_method = "prefix" if steps else "SKIP"

    if generation_method == "prefix":
        walks_per_state = int(steps / n_states / walk_len)
        oracle = StatePrefixEqOracle(inputs, sul, walks_per_state, walk_len)
        cex = oracle.find_cex(copy.deepcopy(mm))
        assert cex is None, "Found CEX on identical machine...??"
    elif generation_method == "rw":
        oracle = RandomWalkEqOracle(inputs, sul, steps, reset_prob=reset_prob)
        cex = oracle.find_cex(copy.deepcopy(mm))
        assert cex is None, "Found CEX on identical machine...??"
    elif generation_method == "tc":  # TODO
        oracle = KWayTransitionCoverageEqOracle(inputs, sul, steps, random_walk_len=walk_len)
        cex = oracle.find_cex(copy.deepcopy(mm))
        assert cex is None, "Found CEX on identical machine...??"
    elif generation_method == "SKIP":
        pass
    else:
        assert False

    traces = sul.traces
    assert not use_single_trace or len(traces) == 1
    original_traces = copy.deepcopy(traces)

    ## sanity check
    for trace in traces:
        mm.reset_to_initial()
        assert mm.step(None) == trace[0]
        for i in range(1, len(trace)):
            letter, out = trace[i][:2]
            o_step = mm.step(letter)
            assert out == o_step, "Difference between trace and lstar model???"
    ## end sanity check

    ## inject glitches into trace

    if glitch_percent and glitch_percent > 0:
        if glitch_percent >= 0.5:
            print(f"WARN: trace amount more than {glitch_percent*100}%, are you sure about that?")
            time.sleep(5)
        total_steps = steps_traces(traces)
        total_num_glitches = int(total_steps * glitch_percent)
        if total_num_glitches <= 0:
            print("WARN: adding one glitch (although we would have gotten zero)")
            total_num_glitches = 1

        indices = range(len(traces))
        weights = [len(trace) / total_steps for trace in traces]
        glitch_counts = random.choices(indices, weights, k=total_num_glitches)
        assert len(glitch_counts) == total_num_glitches

        ### this code DROPS random entries from the trace (there could be different fault injection strategies)
        for i in range(len(traces)):
            glitch_count = sum(1 for index in glitch_counts if index == i)
            if glitch_count == 0:
                continue
            trace = traces[i]
            if glitch_count >= len(trace) - 1:
                print("WARN: dropping entire trace!")
                glitch_count = len(trace) - 1
            glitch_rm_indices = set(random.sample(range(1, len(trace)), k=glitch_count))
            o0 = trace[0]
            traces[i] = [o0] + [
                entry
                for index, entry in enumerate(trace[1:])
                if (index + 1) not in glitch_rm_indices
            ]
        print(
            f"INFO: removed {total_num_glitches} entries from trace with original total length {total_steps}"
        )
        assert steps_traces(traces) == total_steps - total_num_glitches

    ## prepare traces and visualization

    traces_hash = hashlib.md5(str(traces).encode("utf8"), usedforsecurity=False).hexdigest()
    path = Path("MOORE-results") / traces_hash
    path.mkdir(parents=True, exist_ok=True)

    with (path / "info.json").open("w") as f:
        json.dump(
            {
                "original_automaton": file,
                "glitch_percent": glitch_percent,
                "random_seed": seed,
                "num_states": n_states,
                "input_alphabet": inputs,
                "output_alphabet": outputs,
                "original_traces": original_traces,
                "traces": traces,
            },
            f,
        )

    mm.save((path / "RealModel").as_posix())
    if auto_learned is not None:
        algo: str = args.generation_method
        auto_learned.save((path / f"{algo.capitalize()}Learned").as_posix())
        assert bisimilar(auto_learned, mm) is None, "Learned automaton is not correct"
        assert auto_learned.is_minimal(), "Learned automaton is not minimal"
        assert auto_learned.is_input_complete(), "Learned automaton is not input complete"

    print(f"{len(traces)} traces of total length: {steps_traces(traces)}")

    if args.only_generate:
        exit(0)

    ## use simple trace for IO ALERGIA

    simple_moore_traces = [
        ([trace[0]] + [tuple(entry[:2]) for entry in trace[1:]])
        for trace in traces
        if len(trace) > 1
    ]

    ## Learn with IO ALERGIA
    try:
        alergia_learned = run_Alergia(simple_moore_traces, "mdp", print_info=True, eps=0.5)
        alergia_learned.save((path / "alergiaLearned").as_posix())
    except ZeroDivisionError:
        print("WARN: Alergia could not calculate probabilities")

    ## Try different timeouts for PartialMaxSAT Algorithm

    max_states = n_states + 10
    result_det_pmsat_automaton = [None] * (max_states + 1)
    # TODO: this loop could be run in parallel!
    for n in range(len(outputs), max_states + 1):
        # n = n_states + 6
        print(f"Learning with N={n}")
        pmsat_learned, pmsat_learned_stoc, info = run_pmSATLearn(
            traces,
            n,
            automata_type="moore",
            pm_strategy=pmstrat,
            timeout=None,
            print_info=False,
        )
        result_det_pmsat_automaton[n] = pmsat_learned
        name = f"pmsatLearned-{pmstrat}-N{n}-RN{n_states}"
        if pmsat_learned is not None:
            assert pmsat_learned_stoc is not None
            # print("GLITCHES:", info["glitch_steps"])
            print("#glitches:", len(info["glitch_steps"]))
            pmsat_learned.save((path / name).as_posix())
            stoc_name = (path / (name + "-STOC")).as_posix()
            pmsat_learned_stoc.save(stoc_name)
            try:
                with open(stoc_name + ".dot", "r") as f:
                    dot_stoc_automaton = f.read()
                with open(stoc_name + ".dot", "w") as f:
                    for i in range(1, 1000):
                        if f'] t{i}"];' not in dot_stoc_automaton:
                            break
                        dot_stoc_automaton = dot_stoc_automaton.replace(f'] t{i}"];', ']"];')
                    f.write(dot_stoc_automaton.replace('label="!', 'color=red, label="'))
            except FileNotFoundError:
                print("WARN: Could not find file for stochastic edge coloring red")
            cex = bisimilar(mm, pmsat_learned)
            info["bisimilar_base_cex"] = cex
            info["minimal"] = pmsat_learned.is_minimal()
            if cex is not None:
                print("Is not bisimilar, cex:", cex)
            for automaton_index, learned_pmsat_automaton in enumerate(result_det_pmsat_automaton):
                if learned_pmsat_automaton is not None:
                    if bisimilar(pmsat_learned, learned_pmsat_automaton) is None:
                        info["bisimilar_to_pmsat_with_states"] = automaton_index
                        break
            assert info["bisimilar_to_pmsat_with_states"]
        else:
            print("PMSAT - UNSAT")
        with (path / (name + ".json")).open("w") as f:
            json.dump(info, f)
        if info["optimal_solution"] is False:
            print("Breaking from loop due to interrupt or timeout")
            break
        if "cost" in info and info["cost"] == 0:
            print("Found glitchless solution, more states will not change that. Stopping...")
            break


if __name__ == "__main__":
    main()
